-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Úte 02. úno 2016, 02:05
-- Verze serveru: 5.6.15-log
-- Verze PHP: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databáze: `190_hana`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_preferences`
--

CREATE TABLE IF NOT EXISTS `admin_preferences` (
  `id` int(10) unsigned NOT NULL,
  `kod` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_settings`
--

CREATE TABLE IF NOT EXISTS `admin_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email_podpora_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `logo_dodavatele` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `kohana_debug_mode` tinyint(4) NOT NULL DEFAULT '0',
  `smarty_console` tinyint(4) NOT NULL DEFAULT '0',
  `shutdown` tinyint(4) NOT NULL DEFAULT '0',
  `disable_login` int(11) NOT NULL DEFAULT '0',
  `interni_poznamka` text COLLATE utf8_czech_ci,
  `admin_theme_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `admin_settings`
--

INSERT INTO `admin_settings` (`id`, `nazev_dodavatele`, `email_podpora_dodavatele`, `logo_dodavatele`, `kohana_debug_mode`, `smarty_console`, `shutdown`, `disable_login`, `interni_poznamka`, `admin_theme_id`) VALUES
(1, 'dgstudio s.r.o.', 'info@dgstudio.cz', NULL, 0, 0, 0, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_structure`
--

CREATE TABLE IF NOT EXISTS `admin_structure` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module_code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `submodule_code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `module_controller` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `admin_menu_section_id` int(11) NOT NULL,
  `poradi` int(10) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `global_access_level` tinyint(4) NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=78 ;

--
-- Vypisuji data pro tabulku `admin_structure`
--

INSERT INTO `admin_structure` (`id`, `module_code`, `submodule_code`, `module_controller`, `admin_menu_section_id`, `poradi`, `parent_id`, `zobrazit`, `global_access_level`, `available_languages`) VALUES
(1, 'environment', 'module', 'default', 1, 1, 0, 1, 0, 5),
(2, 'environment', 'setting', 'edit/1/1', 3, 15, 1, 1, 0, 5),
(3, 'environment', 'samodules', 'list', 3, 4, 1, 1, 3, 5),
(4, 'environment', 'salanguages', 'list', 3, 14, 1, 0, 0, 1),
(5, 'environment', 'routes', 'list', 3, 6, 1, 1, 3, 1),
(6, 'environment', 'mainsetup', 'edit/1/1', 3, 1, 1, 1, 1, 1),
(7, 'environment', 'inmodules', 'list', 3, 2, 1, 0, 3, 1),
(8, 'environment', 'registry', 'list', 3, 5, 1, 1, 3, 1),
(9, 'page', 'item', 'list', 1, 2, 0, 1, 0, 1),
(10, 'page', 'item', 'list', 3, 1, 9, 1, 0, 1),
(11, 'page', 'unrelated', 'list', 3, 2, 9, 1, 0, 1),
(12, 'static', 'item', 'list', 3, 3, 9, 1, 0, 1),
(13, 'user', 'item', 'list', 1, 25, 0, 1, 0, 1),
(14, 'article', 'item', 'list', 1, 3, 0, 1, 0, 1),
(15, 'product', 'item', 'list', 1, 5, 0, 1, 0, 1),
(16, 'product', 'item', 'list', 2, 2, 15, 1, 0, 1),
(17, 'product', 'category', 'list', 2, 3, 15, 1, 0, 1),
(18, 'product', 'manufacturer', 'list', 2, 5, 15, 1, 0, 1),
(19, 'product', 'order', 'list', 2, 1, 15, 1, 0, 1),
(20, 'product', 'shipping', 'list', 2, 6, 15, 1, 0, 1),
(21, 'product', 'payment', 'list', 2, 7, 15, 1, 0, 1),
(22, 'product', 'price', 'list', 2, 8, 15, 1, 0, 1),
(23, 'product', 'tax', 'list', 2, 9, 15, 1, 0, 1),
(24, 'page', 'system', 'list', 3, 4, 9, 1, 3, 1),
(25, 'email', 'queue', 'list', 1, 26, 0, 1, 0, 1),
(26, 'email', 'queue', 'list', 3, 1, 25, 1, 0, 1),
(27, 'email', 'type', 'list', 3, 2, 25, 1, 0, 1),
(28, 'email', 'receiver', 'list', 3, 3, 25, 1, 0, 1),
(29, 'email', 'smtp', 'edit/1/1', 3, 4, 25, 1, 0, 1),
(30, 'product', 'shopper', 'list', 2, 4, 15, 1, 0, 1),
(31, 'product', 'orderstates', 'list', 2, 10, 15, 1, 0, 1),
(32, 'product', 'eshopsettings', 'edit/1/1', 2, 11, 15, 1, 0, 1),
(33, 'product', 'voucher', 'list', 2, 12, 15, 0, 0, 1),
(34, 'environment', 'langstrings', 'list', 3, 16, 1, 1, 1, 1),
(39, 'box', 'item', 'list', 1, 14, 0, 1, 0, 1),
(43, 'article', 'category', 'list', 2, 1, 14, 0, 0, 1),
(46, 'newsletter', 'item', 'list', 1, 23, 0, 0, 0, 1),
(47, 'newsletter', 'recipient', 'list', 2, 1, 46, 1, 0, 1),
(63, 'reference', 'item', 'list', 1, 22, 0, 1, 0, 1),
(64, 'reference', 'category', 'list', 2, 1, 63, 1, 0, 1),
(65, 'branch', 'item', 'list', 1, 24, 0, 1, 0, 1),
(67, 'branch', 'salesman', 'list', 2, 2, 65, 1, 0, 1),
(68, 'slider', 'slider', 'edit/1/1', 1, 21, 0, 1, 0, 1),
(70, 'slider', 'item', 'list', 2, 1, 68, 1, 0, 1),
(75, 'catalogue', 'item', 'list', 1, 13, 0, 1, 0, 1),
(76, 'catalogue', 'category', 'list', 2, 1, 75, 1, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_structure_data`
--

CREATE TABLE IF NOT EXISTS `admin_structure_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_structure_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `nadpis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`) USING BTREE,
  KEY `admin_module_id` (`admin_structure_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=102 ;

--
-- Vypisuji data pro tabulku `admin_structure_data`
--

INSERT INTO `admin_structure_data` (`id`, `admin_structure_id`, `language_id`, `nazev`, `nadpis`, `title`, `description`, `keywords`, `popis`) VALUES
(1, 1, 1, 'Základní', 'Základní', 'Základní', '', '', NULL),
(6, 6, 1, 'Nastavení projektu', 'Základní nastavení', 'Základní nastavení', '', '', NULL),
(3, 3, 1, 'Struktura administrace', 'Struktura administrace', 'Struktura administrace', '', '', NULL),
(7, 7, 1, 'Instalátor modulů', 'Instalátor modulů', 'Instalátor modulů', '', '', NULL),
(8, 8, 1, 'Registry modulů', 'Registry modulů', 'Registry modulů', '', '', NULL),
(5, 5, 1, 'Záznamy rout', 'Záznamy rout', 'Záznamy rout', '', '', NULL),
(4, 4, 1, 'Nastavení jazyků', 'Nastavení jazyků', 'Nastavení jazyků', '', '', NULL),
(2, 2, 1, 'Obecné nastavení', 'Obecné nastavení', 'Obecné nastavení', '', '', NULL),
(9, 9, 1, 'Stránky', 'Stránky', 'Stránky', '', '', NULL),
(10, 10, 1, 'Stránky ve struktuře menu', 'Stránky ve struktuře menu', 'Stránky ve struktuře menu', '', '', NULL),
(11, 11, 1, 'Nezařazené stránky', 'Nezařazené stránky', 'Nezařazené stránky', '', '', NULL),
(12, 12, 1, 'Statický obsah', 'Statický obsah', 'Statický obsah', '', '', NULL),
(13, 13, 1, 'Uživatelé', 'Uživatelé', 'Uživatelé', '', '', NULL),
(14, 14, 1, 'Novinky', 'Novinky', 'Novinky', '', NULL, NULL),
(24, 24, 1, 'Systémové stránky', 'Systémové stránky', 'Systémové stránky', '', NULL, NULL),
(25, 25, 1, 'Email', 'Email', 'Email', '', NULL, NULL),
(26, 26, 1, 'Fronta emailů', 'Fronta emailů', 'Fronta emailů', '', NULL, NULL),
(27, 27, 1, 'Typy emailů', 'Typy emailů', 'Typy emailů', '', NULL, NULL),
(28, 28, 1, 'Příjemci emailů', 'Příjemci emailů', 'Příjemci emailů', '', NULL, NULL),
(29, 29, 1, 'Nastavení SMTP', 'Nastavení SMTP', 'Nastavení SMTP', '', NULL, NULL),
(34, 34, 1, 'Statické překlady', 'Statické překlady', 'Statické překlady', '', NULL, NULL),
(92, 68, 1, 'Slider', 'Slider', 'Slider', '', NULL, NULL),
(15, 15, 1, 'E-shop', 'E-shop', 'E-shop', '', NULL, NULL),
(16, 16, 1, 'Seznam produktů', 'Seznam produktů', 'Seznam produktů', '', NULL, NULL),
(47, 1, 4, 'Defaults', 'Defaults', 'Defaults', '', NULL, NULL),
(48, 3, 4, 'Administration structure', 'Samodules', 'Samodules', '', NULL, NULL),
(49, 2, 4, 'Default setting', 'Default setting', 'Default setting', '', NULL, NULL),
(62, 39, 1, 'Boxy', 'Boxy', 'Boxy', '', NULL, NULL),
(66, 43, 1, 'Kategorie', 'Kategorie', 'Kategorie', '', NULL, NULL),
(94, 70, 1, 'Slajdy', 'Slajdy', 'Slajdy', '', NULL, NULL),
(89, 65, 1, 'Kontakty', 'Pobočky', 'Pobočky', '', NULL, NULL),
(69, 46, 1, 'Newsletter', 'Newsletter', 'Newsletter', '', NULL, NULL),
(70, 47, 1, 'Příjemci', 'Příjemci', 'Příjemci', '', NULL, NULL),
(91, 67, 1, 'Lidé', 'Lidé', 'Lidé', '', NULL, NULL),
(86, 63, 1, 'Reference', 'Reference', 'Reference', '', NULL, NULL),
(87, 64, 1, 'Kategorie', 'Kategorie', 'Kategorie', '', NULL, NULL),
(99, 75, 1, 'Katalog', 'Katalog', 'Katalog', '', NULL, NULL),
(100, 76, 1, 'Kategorie', 'Kategorie', 'Kategorie', '', NULL, NULL),
(17, 17, 1, 'Kategorie produktů', 'Kategorie produktů', 'Kategorie produktů', '', NULL, NULL),
(18, 18, 1, 'Výrobci', 'Výrobci', 'Výrobci', '', NULL, NULL),
(19, 19, 1, 'Objednávky', 'Objednávky', 'Objednávky', '', NULL, NULL),
(20, 20, 1, 'Doprava', 'Doprava', 'Doprava', '', NULL, NULL),
(21, 21, 1, 'Platba', 'Platba', 'Platba', '', NULL, NULL),
(22, 22, 1, 'Cenové skupiny', 'Cenové skupiny', 'Cenové skupiny', '', NULL, NULL),
(23, 23, 1, 'DPH', 'DPH', 'DPH', '', NULL, NULL),
(30, 30, 1, 'Uživatelé', 'Uživatelé', 'Uživatelé', '', NULL, NULL),
(31, 31, 1, 'Stavy objednávek', 'Stavy objednávek', 'Stavy objednávek', '', NULL, NULL),
(32, 32, 1, 'Nastavení eshopu', 'Nastavení eshopu', 'Nastavení eshopu', '', NULL, NULL),
(33, 33, 1, 'Slevové kupóny', 'Slevové kupóny', 'Slevové kupóny', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `admin_themes`
--

CREATE TABLE IF NOT EXISTS `admin_themes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nazev` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  `nazev_css` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `inverse` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=8 ;

--
-- Vypisuji data pro tabulku `admin_themes`
--

INSERT INTO `admin_themes` (`id`, `nazev`, `popis`, `nazev_css`, `inverse`) VALUES
(1, 'Základní', NULL, 'bootstrap', 0),
(2, 'Darkly', NULL, 'bootstrap-darkly', 0),
(3, 'Lumen', NULL, 'bootstrap-lumen', 0),
(4, 'Paper', NULL, 'bootstrap-paper', 0),
(5, 'Sandstone', NULL, 'bootstrap-sandstone', 0),
(6, 'Yeti', NULL, 'bootstrap-yeti', 1),
(7, 'Cosmo', NULL, 'bootstrap-cosmo', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `poradi` int(11) NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `author` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `show_contactform` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `articles_article_categories`
--

CREATE TABLE IF NOT EXISTS `articles_article_categories` (
  `article_id` int(10) unsigned NOT NULL,
  `article_category_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_categories`
--

CREATE TABLE IF NOT EXISTS `article_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `poradi` int(10) unsigned NOT NULL,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `nazev` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_category_data`
--

CREATE TABLE IF NOT EXISTS `article_category_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `article_category_id` int(10) unsigned NOT NULL,
  `route_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `nadpis` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `popis` text COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_data`
--

CREATE TABLE IF NOT EXISTS `article_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nadpis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `autor` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`,`article_id`) USING BTREE,
  KEY `article_id` (`article_id`) USING BTREE,
  KEY `route_id` (`route_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_photos`
--

CREATE TABLE IF NOT EXISTS `article_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `article_id` (`article_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `article_photo_data`
--

CREATE TABLE IF NOT EXISTS `article_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `article_photo_id` (`article_photo_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `boxes`
--

CREATE TABLE IF NOT EXISTS `boxes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `photo_src` varchar(256) COLLATE utf8_czech_ci DEFAULT NULL,
  `background_src` varchar(256) COLLATE utf8_czech_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `box_data`
--

CREATE TABLE IF NOT EXISTS `box_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `box_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  `nazev` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  `link` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `link_nazev` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `branches`
--

CREATE TABLE IF NOT EXISTS `branches` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `address` text COLLATE utf8_czech_ci,
  `lng` decimal(9,7) NOT NULL,
  `lat` decimal(9,7) NOT NULL,
  `ic` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `dic` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `icp` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `branch_data`
--

CREATE TABLE IF NOT EXISTS `branch_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `branch_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nadpis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `comment` text COLLATE utf8_czech_ci,
  `phone` text COLLATE utf8_czech_ci,
  `fax` text COLLATE utf8_czech_ci,
  `email` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `opening_hours` text COLLATE utf8_czech_ci,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `email_queue`
--

CREATE TABLE IF NOT EXISTS `email_queue` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `queue_to_email` varchar(255) NOT NULL,
  `queue_to_name` varchar(255) DEFAULT NULL,
  `queue_cc_email` varchar(255) DEFAULT NULL,
  `queue_cc_name` varchar(255) DEFAULT NULL,
  `email_queue_body_id` int(11) NOT NULL,
  `queue_sent` tinyint(4) NOT NULL DEFAULT '0',
  `queue_sent_date` datetime DEFAULT NULL,
  `queue_priority` int(11) NOT NULL DEFAULT '0',
  `queue_date_to_be_send` datetime DEFAULT NULL,
  `queue_create_date` datetime NOT NULL,
  `queue_errors_count` tinyint(4) NOT NULL DEFAULT '0',
  `queue_error` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `email_queue_bodies`
--

CREATE TABLE IF NOT EXISTS `email_queue_bodies` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `queue_subject` varchar(255) DEFAULT NULL,
  `queue_from_email` varchar(255) NOT NULL,
  `queue_from_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `queue_body` text NOT NULL,
  `queue_attached_file` varchar(255) DEFAULT NULL,
  `queue_newsletter_id` int(11) DEFAULT NULL,
  `queue_shopper_id` bigint(20) DEFAULT NULL,
  `queue_branch_id` bigint(20) DEFAULT NULL,
  `queue_order_id` bigint(20) DEFAULT NULL,
  `queue_user_id` bigint(20) DEFAULT NULL,
  `queue_email_type_id` int(11) DEFAULT NULL,
  `queue_send_by_cron` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email_type_id` (`queue_email_type_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `email_receivers`
--

CREATE TABLE IF NOT EXISTS `email_receivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `email_settings`
--

CREATE TABLE IF NOT EXISTS `email_settings` (
  `id` int(11) NOT NULL,
  `mailer` varchar(16) NOT NULL DEFAULT 'mail',
  `host` varchar(32) NOT NULL DEFAULT 'localhost',
  `port` int(11) NOT NULL DEFAULT '25',
  `SMTPSecure` varchar(32) DEFAULT NULL,
  `SMTPAuth` tinyint(4) NOT NULL DEFAULT '0',
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `SMTPDebug` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `email_settings`
--

INSERT INTO `email_settings` (`id`, `mailer`, `host`, `port`, `SMTPSecure`, `SMTPAuth`, `username`, `password`, `SMTPDebug`) VALUES
(1, 'smtp', 'mail.dghost.cz', 25, '', 1, 'smtp@dghost.cz', 'smtpmageror', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `email_types`
--

CREATE TABLE IF NOT EXISTS `email_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `template` varchar(255) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `from_nazev` varchar(255) DEFAULT '',
  `from_email` varchar(255) DEFAULT '',
  `use_email_queue` tinyint(4) NOT NULL DEFAULT '0',
  `send_by_cron` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `email_types_receivers`
--

CREATE TABLE IF NOT EXISTS `email_types_receivers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_type_id` int(11) NOT NULL,
  `email_receiver_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email_type_id` (`email_type_id`,`email_receiver_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `eshop_settings`
--

CREATE TABLE IF NOT EXISTS `eshop_settings` (
  `id` int(11) NOT NULL,
  `billing_data_nazev` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_email` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_ulice` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_mesto` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_psc` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_ic` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_dic` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_telefon` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_fax` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_banka` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_iban` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_cislo_uctu` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_konst_s` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_spec_s` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `billing_data_swift` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `present_enabled` tinyint(4) NOT NULL,
  `present_price_threshold` decimal(10,0) NOT NULL,
  `billing_data_due_date` int(11) DEFAULT NULL,
  `shipping_free_threshold` decimal(6,0) DEFAULT NULL,
  `first_purchase_discount` decimal(4,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `eshop_settings`
--

INSERT INTO `eshop_settings` (`id`, `billing_data_nazev`, `billing_data_email`, `billing_data_ulice`, `billing_data_mesto`, `billing_data_psc`, `billing_data_ic`, `billing_data_dic`, `billing_data_telefon`, `billing_data_fax`, `billing_data_banka`, `billing_data_iban`, `billing_data_cislo_uctu`, `billing_data_konst_s`, `billing_data_spec_s`, `billing_data_swift`, `present_enabled`, `present_price_threshold`, `billing_data_due_date`, `shipping_free_threshold`, `first_purchase_discount`) VALUES
(0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktura tabulky `gift_boxes`
--

CREATE TABLE IF NOT EXISTS `gift_boxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `invoice_settings`
--

CREATE TABLE IF NOT EXISTS `invoice_settings` (
  `id` int(11) NOT NULL,
  `next_invoice_code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `invoice_settings`
--

INSERT INTO `invoice_settings` (`id`, `next_invoice_code`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `language_strings`
--

CREATE TABLE IF NOT EXISTS `language_strings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `read_only` tinyint(4) NOT NULL DEFAULT '0',
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `language_string_data`
--

CREATE TABLE IF NOT EXISTS `language_string_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `language_string_id` int(11) NOT NULL,
  `string` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_string_id`) USING BTREE,
  KEY `language_id_2` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `manufacturers`
--

CREATE TABLE IF NOT EXISTS `manufacturers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT '',
  `photo` varchar(255) COLLATE utf8_czech_ci DEFAULT '',
  `poradi` int(11) NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `manufacturer_data`
--

CREATE TABLE IF NOT EXISTS `manufacturer_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`),
  KEY `manufacturer_id` (`manufacturer_id`),
  KEY `language_id` (`language_id`),
  KEY `route_id` (`route_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kod` varchar(20) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `verze` varchar(10) COLLATE utf8_czech_ci DEFAULT NULL,
  `datum` date DEFAULT NULL,
  `autor` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci,
  `poznamka` text COLLATE utf8_czech_ci,
  `poradi` int(11) NOT NULL DEFAULT '0',
  `admin_zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `available` tinyint(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `kod` (`kod`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=14 ;

--
-- Vypisuji data pro tabulku `modules`
--

INSERT INTO `modules` (`id`, `kod`, `nazev`, `verze`, `datum`, `autor`, `url`, `email`, `popis`, `poznamka`, `poradi`, `admin_zobrazit`, `available`) VALUES
(1, 'page', 'Stránky', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul zobrazující klasický statický obsah ve stránkách.', NULL, 1, 1, 1),
(2, 'link', 'Odkazy', '1', NULL, 'Pavel Herink', NULL, NULL, 'Odkaz na externí stránku.', NULL, 2, 1, 1),
(3, 'article', 'Články', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul článků a novinek.', NULL, 3, 1, 1),
(4, 'contact', 'Kontakty', '1', NULL, 'Pavel Herink', NULL, NULL, 'Modul kontaktního formuláře.', NULL, 4, 1, 1),
(5, 'search', 'Vyhledávání', '1', NULL, 'Pavel Herink', NULL, NULL, NULL, NULL, 6, 0, 1),
(6, 'newsletter', 'Newsletter', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, 0, 1),
(7, 'product', 'Produkty', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 1, 1),
(8, 'sitemap', 'Mapa stránek', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, 1, 0),
(9, 'reference', 'Reference', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, 1, 1),
(11, 'shoppingcart', 'Košík', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, 0, 1),
(10, 'order', 'Objednávka', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, 0, 1),
(12, 'catalogue', 'Katalog', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 1, 1),
(13, 'user', 'Uživatel - front', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 13, 0, 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `module_actions`
--

CREATE TABLE IF NOT EXISTS `module_actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int(11) NOT NULL,
  `kod` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `povoleno` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`,`povoleno`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `newsletters`
--

CREATE TABLE IF NOT EXISTS `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `generovan` tinyint(4) NOT NULL DEFAULT '0',
  `sent` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `newsletter_data`
--

CREATE TABLE IF NOT EXISTS `newsletter_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `newsletter_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `autor` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`,`newsletter_id`),
  KEY `newsletter_id` (`newsletter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `newsletter_recipients`
--

CREATE TABLE IF NOT EXISTS `newsletter_recipients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hash` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `allowed` tinyint(3) unsigned NOT NULL,
  `shopper_id` int(10) unsigned NOT NULL,
  `email` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shopper_id` (`shopper_id`),
  KEY `allowed` (`allowed`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mode` varchar(16) NOT NULL DEFAULT 'eshop',
  `order_code` char(13) NOT NULL,
  `order_code_invoice` varchar(12) DEFAULT NULL,
  `order_date` datetime NOT NULL,
  `order_date_finished` date DEFAULT NULL,
  `order_delivery_date` datetime DEFAULT NULL,
  `order_date_tax` datetime DEFAULT NULL,
  `order_date_payment` datetime DEFAULT NULL,
  `order_payment_id` tinyint(4) NOT NULL DEFAULT '0',
  `order_shipping_id` tinyint(4) NOT NULL,
  `order_const_symbol` varchar(4) NOT NULL DEFAULT '0',
  `order_state_id` tinyint(4) NOT NULL DEFAULT '0',
  `order_price_no_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_price_lower_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_price_higher_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_no_vat_rate` int(11) NOT NULL DEFAULT '0',
  `order_lower_vat_rate` int(11) NOT NULL DEFAULT '10',
  `order_higher_vat_rate` int(11) NOT NULL DEFAULT '20',
  `order_lower_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_higher_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_total_without_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_total_with_vat` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_shipping_price` decimal(6,2) NOT NULL DEFAULT '0.00',
  `order_payment_price` decimal(6,2) NOT NULL,
  `order_total` decimal(9,2) NOT NULL,
  `order_voucher_id` int(11) DEFAULT NULL,
  `order_voucher_discount` decimal(10,2) DEFAULT NULL,
  `order_discount` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_correction` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_total_CZK` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_weight` decimal(9,2) NOT NULL DEFAULT '0.00',
  `order_shopper_id` int(11) DEFAULT '0',
  `order_shopper_branch` int(11) DEFAULT '0',
  `order_shopper_name` varchar(255) NOT NULL,
  `order_shopper_code` char(6) DEFAULT NULL,
  `order_shopper_email` varchar(100) NOT NULL,
  `order_shopper_phone` varchar(20) NOT NULL,
  `order_shopper_ic` varchar(64) DEFAULT NULL,
  `order_shopper_dic` varchar(20) DEFAULT NULL,
  `order_shopper_street` varchar(50) DEFAULT NULL,
  `order_shopper_city` varchar(50) DEFAULT NULL,
  `order_shopper_zip` varchar(10) DEFAULT NULL,
  `order_shopper_note` text NOT NULL,
  `order_branch_name` varchar(255) DEFAULT NULL,
  `order_branch_code` char(6) DEFAULT NULL,
  `order_branch_street` varchar(50) DEFAULT NULL,
  `order_branch_city` varchar(50) DEFAULT NULL,
  `order_branch_zip` varchar(50) DEFAULT NULL,
  `order_branch_phone` varchar(20) DEFAULT NULL,
  `order_branch_email` varchar(100) DEFAULT NULL,
  `export` tinyint(4) NOT NULL DEFAULT '0',
  `last_modified` datetime NOT NULL,
  `order_shopper_custommer_code` varchar(255) DEFAULT NULL,
  `post_track_trace_code` varchar(255) DEFAULT NULL,
  `is_payu` tinyint(4) NOT NULL DEFAULT '0',
  `payu_session_code` varchar(255) DEFAULT NULL,
  `payu_status_code` varchar(255) DEFAULT NULL,
  `payu_status_message` varchar(255) DEFAULT NULL,
  `payu_error_message` varchar(255) DEFAULT NULL,
  `gp_webpay_status` int(2) DEFAULT NULL,
  `gp_webpay_prcode` int(11) DEFAULT NULL,
  `gp_webpay_srcode` int(11) DEFAULT NULL,
  `gp_webpay_text_status` varchar(255) DEFAULT NULL,
  `gift_box_code` varchar(255) DEFAULT NULL,
  `engine_id` int(11) DEFAULT NULL,
  `actions` tinyint(4) NOT NULL,
  `order_montaz_price` decimal(9,2) NOT NULL,
  `order_montaz` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Vypisuji data pro tabulku `orders`
--

INSERT INTO `orders` (`id`, `mode`, `order_code`, `order_code_invoice`, `order_date`, `order_date_finished`, `order_delivery_date`, `order_date_tax`, `order_date_payment`, `order_payment_id`, `order_shipping_id`, `order_const_symbol`, `order_state_id`, `order_price_no_vat`, `order_price_lower_vat`, `order_price_higher_vat`, `order_no_vat_rate`, `order_lower_vat_rate`, `order_higher_vat_rate`, `order_lower_vat`, `order_higher_vat`, `order_total_without_vat`, `order_total_with_vat`, `order_shipping_price`, `order_payment_price`, `order_total`, `order_voucher_id`, `order_voucher_discount`, `order_discount`, `order_correction`, `order_total_CZK`, `order_weight`, `order_shopper_id`, `order_shopper_branch`, `order_shopper_name`, `order_shopper_code`, `order_shopper_email`, `order_shopper_phone`, `order_shopper_ic`, `order_shopper_dic`, `order_shopper_street`, `order_shopper_city`, `order_shopper_zip`, `order_shopper_note`, `order_branch_name`, `order_branch_code`, `order_branch_street`, `order_branch_city`, `order_branch_zip`, `order_branch_phone`, `order_branch_email`, `export`, `last_modified`, `order_shopper_custommer_code`, `post_track_trace_code`, `is_payu`, `payu_session_code`, `payu_status_code`, `payu_status_message`, `payu_error_message`, `gp_webpay_status`, `gp_webpay_prcode`, `gp_webpay_srcode`, `gp_webpay_text_status`, `gift_box_code`, `engine_id`, `actions`, `order_montaz_price`, `order_montaz`) VALUES
(18, 'eshop', '', NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, 0, 0, '0', 0, '0.00', '0.00', '0.00', 0, 10, 20, '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', NULL, NULL, '0.00', '0.00', '0.00', '0.00', 0, 0, '', NULL, '', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '2016-02-02 01:30:43', NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0.00', 0);

-- --------------------------------------------------------

--
-- Struktura tabulky `order_items`
--

CREATE TABLE IF NOT EXISTS `order_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `varianta_popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `varianta_id` int(11) NOT NULL,
  `jednotka` char(2) COLLATE utf8_czech_ci NOT NULL,
  `hmotnost` decimal(5,2) NOT NULL,
  `pocet_na_sklade` decimal(6,2) NOT NULL,
  `min_order_quantity` decimal(3,2) DEFAULT NULL,
  `tax_code` varchar(25) CHARACTER SET utf8 NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `units` decimal(6,2) NOT NULL,
  `price_without_tax` decimal(10,2) NOT NULL,
  `price_with_tax` decimal(10,2) NOT NULL,
  `total_price_with_tax` decimal(10,2) NOT NULL,
  `item_change` tinyint(4) DEFAULT '0',
  `total_weight` decimal(6,2) NOT NULL DEFAULT '0.00',
  `gift` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_item_changes`
--

CREATE TABLE IF NOT EXISTS `order_item_changes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `change_order` bigint(20) NOT NULL,
  `change_item` bigint(20) NOT NULL DEFAULT '0',
  `change_date` datetime DEFAULT NULL,
  `change_units_from` decimal(6,2) NOT NULL DEFAULT '0.00',
  `change_units_to` decimal(6,2) NOT NULL DEFAULT '0.00',
  `change_type` varchar(10) NOT NULL DEFAULT 'internal',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_states`
--

CREATE TABLE IF NOT EXISTS `order_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `order_state_type_id` int(11) NOT NULL,
  `send_mail` tinyint(4) NOT NULL DEFAULT '0',
  `readonly` tinyint(4) NOT NULL DEFAULT '0',
  `poradi` tinyint(4) NOT NULL,
  `smazano` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order_state_type_id` (`order_state_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_state_data`
--

CREATE TABLE IF NOT EXISTS `order_state_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_state_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email_text` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `order_state_id` (`order_state_id`,`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `order_state_types`
--

CREATE TABLE IF NOT EXISTS `order_state_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `poradi` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `owner_data`
--

CREATE TABLE IF NOT EXISTS `owner_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `default_title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `default_description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `default_keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `firma` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `ulice` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `mesto` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `psc` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `stat` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ic` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `dic` varchar(16) COLLATE utf8_czech_ci DEFAULT NULL,
  `tel` varchar(32) COLLATE utf8_czech_ci DEFAULT NULL,
  `fax` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `www` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `ga_script` text COLLATE utf8_czech_ci NOT NULL,
  `zapis` text COLLATE utf8_czech_ci,
  `facebook` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `google` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `youtube` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `linkedin` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `ior` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=2 ;

--
-- Vypisuji data pro tabulku `owner_data`
--

INSERT INTO `owner_data` (`id`, `default_title`, `default_description`, `default_keywords`, `firma`, `ulice`, `mesto`, `psc`, `stat`, `copyright`, `ic`, `dic`, `tel`, `fax`, `email`, `www`, `ga_script`, `zapis`, `facebook`, `google`, `youtube`, `linkedin`, `ior`) VALUES
(1, '', '', '', '', '', '', '', NULL, '', '', '', '', '', '', NULL, '', '', '', '', '', '', 'OPkHdz5AYttQmeeB6tQmIe5Bs');

-- --------------------------------------------------------

--
-- Struktura tabulky `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_category_id` int(11) NOT NULL,
  `poradi` int(10) unsigned NOT NULL,
  `parent_id` int(11) NOT NULL,
  `indexpage` tinyint(4) NOT NULL,
  `new_window` tinyint(4) NOT NULL DEFAULT '0',
  `show_in_menu` tinyint(4) DEFAULT '1',
  `direct_to_sublink` tinyint(4) NOT NULL DEFAULT '0',
  `show_in_submenu` tinyint(4) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `show_contactform` tinyint(4) NOT NULL DEFAULT '0',
  `photo_src` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `show_visitform` tinyint(4) NOT NULL DEFAULT '0',
  `on_homepage` tinyint(4) NOT NULL,
  `unclickable` tinyint(1) NOT NULL,
  `product_category_id` int(10) unsigned NOT NULL,
  `in_header` tinyint(1) NOT NULL,
  `light_heading` tinyint(1) NOT NULL,
  `show_submenu` tinyint(1) NOT NULL,
  `show_map` tinyint(1) NOT NULL,
  `header_right` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `page_category_id` (`page_category_id`) USING BTREE,
  KEY `show_in_menu` (`show_in_menu`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `page_data`
--

CREATE TABLE IF NOT EXISTS `page_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `nadpis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `podnadpis` varchar(256) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `url` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `link_text` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`) USING BTREE,
  KEY `page_id` (`page_id`) USING BTREE,
  KEY `route_id` (`route_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `page_photos`
--

CREATE TABLE IF NOT EXISTS `page_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `page_id` (`page_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `page_photo_data`
--

CREATE TABLE IF NOT EXISTS `page_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_photo_id` (`page_photo_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cena` decimal(6,2) NOT NULL,
  `typ` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `poradi` int(11) NOT NULL,
  `predem` tinyint(4) NOT NULL DEFAULT '0',
  `payu` tinyint(4) NOT NULL DEFAULT '0',
  `icon` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `payment_type_id` int(5) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payments_shippings`
--

CREATE TABLE IF NOT EXISTS `payments_shippings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_id` (`shipping_id`,`payment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=FIXED AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payment_data`
--

CREATE TABLE IF NOT EXISTS `payment_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `payment_id` (`payment_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `payment_types`
--

CREATE TABLE IF NOT EXISTS `payment_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `special` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `price_categories`
--

CREATE TABLE IF NOT EXISTS `price_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `price_type_id` int(11) NOT NULL,
  `hodnota` tinyint(4) NOT NULL COMMENT 'pripadna procentni hodnota',
  `zaradit_zakaznika_od` decimal(8,0) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `price_type_id` (`price_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `price_categories_products`
--

CREATE TABLE IF NOT EXISTS `price_categories_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price_category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `cena` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `price_category_id` (`price_category_id`),
  KEY `price_category_id_2` (`price_category_id`,`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=1458 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `price_types`
--

CREATE TABLE IF NOT EXISTS `price_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `kratky_popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `main_code` varchar(128) COLLATE utf8_czech_ci NOT NULL,
  `jednotka` varchar(4) COLLATE utf8_czech_ci DEFAULT 'ks',
  `rok_vyroby` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `hmotnost` decimal(7,3) DEFAULT '0.000',
  `pocet_na_sklade` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `min_order_quantity` decimal(3,2) DEFAULT NULL,
  `puvodni_cena` decimal(10,2) NOT NULL,
  `poradi` int(11) NOT NULL,
  `top` tinyint(4) NOT NULL DEFAULT '0',
  `tax_id` int(11) NOT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `import_type` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `original` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `product_expedition_id` int(11) DEFAULT NULL,
  `guarantee` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '0',
  `percentage_discount` decimal(4,2) NOT NULL DEFAULT '0.00',
  `ignore_discount` tinyint(4) NOT NULL DEFAULT '0',
  `new_imported` tinyint(4) NOT NULL DEFAULT '0',
  `imported` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `gift` tinyint(4) NOT NULL DEFAULT '0',
  `gift_threshold_price` decimal(8,0) DEFAULT NULL,
  `product_action_type` tinyint(4) NOT NULL DEFAULT '0',
  `new` tinyint(4) NOT NULL DEFAULT '0',
  `sale_off` tinyint(4) NOT NULL DEFAULT '0',
  `producer_sale_off` tinyint(4) NOT NULL DEFAULT '0',
  `youtube_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `stroj_id` int(11) NOT NULL,
  `prefered` tinyint(4) DEFAULT NULL,
  `action` tinyint(4) DEFAULT '0',
  `altus_jednotka` varchar(20) COLLATE utf8_czech_ci DEFAULT NULL,
  `import_error` tinyint(4) NOT NULL DEFAULT '0',
  `import_note` text COLLATE utf8_czech_ci,
  `base_product` tinyint(1) NOT NULL DEFAULT '1',
  `base_product_id` int(11) DEFAULT NULL,
  `varianta` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `main_photo_size` int(11) NOT NULL DEFAULT '0',
  `gallery_photos_size` int(11) NOT NULL DEFAULT '0',
  `main_photo_uploaded` datetime DEFAULT NULL,
  `gallery_photos_uploaded` datetime DEFAULT NULL,
  `has_variants` tinyint(1) NOT NULL DEFAULT '0',
  `rating` decimal(10,2) DEFAULT NULL,
  `rating_count` int(11) DEFAULT NULL,
  `smazano` tinyint(1) NOT NULL DEFAULT '0',
  `doplnek` tinyint(4) NOT NULL,
  `strong` tinyint(1) NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manufacturer_id` (`manufacturer_id`),
  KEY `tax_id` (`tax_id`),
  KEY `import_type` (`import_type`),
  KEY `original` (`original`),
  KEY `main_code_ix` (`main_code`) USING BTREE,
  KEY `base_product_ix` (`base_product`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `products_related`
--

CREATE TABLE IF NOT EXISTS `products_related` (
  `product_id` int(11) NOT NULL,
  `related_product_id` int(11) NOT NULL,
  `relation_type` int(11) NOT NULL COMMENT '0 - Accessories, 1 - Alternative product, 2 - Incompatible product, 3 - Related Product',
  `note` varchar(255) DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`,`related_product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `products_stores`
--

CREATE TABLE IF NOT EXISTS `products_stores` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `quantity` decimal(11,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`product_id`,`store_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_categories`
--

CREATE TABLE IF NOT EXISTS `product_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `photo_src_left` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `photo_src_right` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `priorita` tinyint(4) NOT NULL DEFAULT '0',
  `special_code` varchar(16) COLLATE utf8_czech_ci NOT NULL COMMENT 'specialni kategorie (novinky, akce, apod.)',
  `class` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `imported` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_categories_products`
--

CREATE TABLE IF NOT EXISTS `product_categories_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `product_category_id` (`product_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=1908 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_category_data`
--

CREATE TABLE IF NOT EXISTS `product_category_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_full` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_jedno` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_menu` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_paticka` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `uvodni_popis_levy` text COLLATE utf8_czech_ci,
  `uvodni_popis_pravy` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `zobrazit_carousel` tinyint(4) NOT NULL DEFAULT '1',
  `zobrazit_na_homepage` tinyint(4) NOT NULL,
  `price_from` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `no_show_products` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`),
  KEY `product_category_id` (`product_category_id`),
  KEY `language_id` (`language_id`),
  KEY `route_id` (`route_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_data`
--

CREATE TABLE IF NOT EXISTS `product_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zobrazit_carousel` tinyint(4) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL DEFAULT '0',
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `nazev_doplnek` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `uvodni_popis` text COLLATE utf8_czech_ci,
  `popis` text COLLATE utf8_czech_ci,
  `odborne_informace` text COLLATE utf8_czech_ci,
  `baleni` text COLLATE utf8_czech_ci,
  `akce_text` text COLLATE utf8_czech_ci,
  `k_prodeji` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`),
  KEY `language_id` (`language_id`),
  KEY `route_id` (`route_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_files`
--

CREATE TABLE IF NOT EXISTS `product_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `file_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `product_id` (`product_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=155 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_file_data`
--

CREATE TABLE IF NOT EXISTS `product_file_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_file_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_file_id` (`product_file_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=155 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_photos`
--

CREATE TABLE IF NOT EXISTS `product_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `uploaded` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=198 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_photo_data`
--

CREATE TABLE IF NOT EXISTS `product_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_photo_id` (`product_photo_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=198 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_ratings`
--

CREATE TABLE IF NOT EXISTS `product_ratings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) unsigned NOT NULL,
  `rating` int(10) unsigned NOT NULL,
  `comment` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_slideshows`
--

CREATE TABLE IF NOT EXISTS `product_slideshows` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` int(11) NOT NULL DEFAULT '0',
  `uploaded` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1059 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_slideshow_data`
--

CREATE TABLE IF NOT EXISTS `product_slideshow_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_slideshow_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_photo_id` (`product_slideshow_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1061 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_variants`
--

CREATE TABLE IF NOT EXISTS `product_variants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `in_stock` int(11) NOT NULL COMMENT 'Na skladě',
  `code` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `doplnek` tinyint(4) NOT NULL DEFAULT '0',
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  `tax_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`,`zobrazit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `product_variant_data`
--

CREATE TABLE IF NOT EXISTS `product_variant_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL,
  `product_variant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `references`
--

CREATE TABLE IF NOT EXISTS `references` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reference_category_id` int(10) unsigned NOT NULL,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `photo_src` varchar(256) DEFAULT NULL,
  `homepage` tinyint(1) NOT NULL,
  `photo_gallery` tinyint(1) NOT NULL,
  `date` date DEFAULT NULL,
  `header_right` tinyint(1) NOT NULL,
  `show_contactform` tinyint(1) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_categories`
--

CREATE TABLE IF NOT EXISTS `reference_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `photo_src` varchar(255) DEFAULT NULL,
  `header_right` tinyint(1) NOT NULL,
  `show_contactform` tinyint(1) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_category_data`
--

CREATE TABLE IF NOT EXISTS `reference_category_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reference_category_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `nazev` varchar(255) NOT NULL,
  `route_id` int(10) unsigned NOT NULL,
  `popis` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_data`
--

CREATE TABLE IF NOT EXISTS `reference_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reference_id` int(10) unsigned NOT NULL,
  `route_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `nazev` varchar(256) NOT NULL,
  `nadpis` varchar(256) NOT NULL,
  `main_nadpis` varchar(256) NOT NULL,
  `popis` text,
  `uvodni_popis` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_photos`
--

CREATE TABLE IF NOT EXISTS `reference_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `poradi` int(11) NOT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '1',
  `photo_src` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `ext` char(4) COLLATE utf8_czech_ci NOT NULL,
  `reference_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `reference_id` (`reference_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `reference_photo_data`
--

CREATE TABLE IF NOT EXISTS `reference_photo_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_photo_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reference_photo_id` (`reference_photo_id`) USING BTREE,
  KEY `language_id` (`language_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Vypisuji data pro tabulku `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`) VALUES
(1, 'login', 'Login privileges, granted after account confirmation'),
(2, 'admin', 'Administrative user, has access to everything.'),
(3, 'global_admin', 'Global administrator.');

-- --------------------------------------------------------

--
-- Struktura tabulky `roles_users`
--

CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vypisuji data pro tabulku `roles_users`
--

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
(7, 1),
(8, 1),
(5, 2),
(7, 2),
(8, 2),
(5, 3),
(7, 3),
(8, 3);

-- --------------------------------------------------------

--
-- Struktura tabulky `routes`
--

CREATE TABLE IF NOT EXISTS `routes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev_seo` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `module_id` int(11) NOT NULL,
  `module_action` varchar(64) COLLATE utf8_czech_ci NOT NULL DEFAULT 'index',
  `param_id1` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `baselang_route_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `read_only` tinyint(4) NOT NULL DEFAULT '0',
  `internal` tinyint(4) NOT NULL DEFAULT '0',
  `searcheable` tinyint(4) NOT NULL DEFAULT '1',
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `nazev_seo_old` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  `zobrazit` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `page_type_id` (`module_id`) USING BTREE,
  KEY `zobrazit` (`zobrazit`) USING BTREE,
  KEY `baselang_route_id` (`baselang_route_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `salesmans`
--

CREATE TABLE IF NOT EXISTS `salesmans` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `available_languages` int(10) unsigned NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `phone` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `photo_src` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `salesmans_branches`
--

CREATE TABLE IF NOT EXISTS `salesmans_branches` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `salesman_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `salesman_data`
--

CREATE TABLE IF NOT EXISTS `salesman_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `salesman_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  `role` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `submodule_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value_code` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value_subcode_1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `value_subcode_2` varchar(255) CHARACTER SET utf8 NOT NULL,
  `poradi` int(11) NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_code` (`module_code`) USING BTREE,
  KEY `submodule_code` (`submodule_code`) USING BTREE,
  KEY `value_code` (`value_code`) USING BTREE
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Struktura tabulky `shippings`
--

CREATE TABLE IF NOT EXISTS `shippings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cena` decimal(6,2) NOT NULL,
  `poradi` int(11) NOT NULL,
  `cenove_hladiny` tinyint(4) NOT NULL DEFAULT '0',
  `hmotnost_od` int(11) DEFAULT NULL,
  `hmotnost_do` int(11) DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shipping_data`
--

CREATE TABLE IF NOT EXISTS `shipping_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `shipping_id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `popis` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `zobrazit` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_id` (`shipping_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shipping_pricelevels`
--

CREATE TABLE IF NOT EXISTS `shipping_pricelevels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shipping_id` int(11) NOT NULL,
  `level` decimal(8,0) NOT NULL,
  `value` decimal(8,0) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_id` (`shipping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shoppers`
--

CREATE TABLE IF NOT EXISTS `shoppers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `nazev` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `nazev_organizace` varchar(255) DEFAULT NULL,
  `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `telefon` varchar(32) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `ulice` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `mesto` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `psc` char(5) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `ic` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `dic` varchar(64) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `datum_registrace` datetime DEFAULT NULL,
  `price_category_id` int(11) NOT NULL,
  `order_total` decimal(12,2) NOT NULL,
  `logins` int(10) NOT NULL DEFAULT '0',
  `last_login` int(10) DEFAULT NULL,
  `newsletter` tinyint(4) NOT NULL DEFAULT '0',
  `smazano` tinyint(4) DEFAULT '0',
  `action_first_purchase` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `price_category_id` (`price_category_id`,`smazano`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `shopper_branches`
--

CREATE TABLE IF NOT EXISTS `shopper_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(64) COLLATE utf8_czech_ci NOT NULL DEFAULT '0',
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_czech_ci DEFAULT NULL,
  `telefon` varchar(32) COLLATE utf8_czech_ci DEFAULT NULL,
  `ulice` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `mesto` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `psc` char(5) COLLATE utf8_czech_ci NOT NULL,
  `shopper_id` int(11) NOT NULL,
  `smazano` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `sliders`
--

CREATE TABLE IF NOT EXISTS `sliders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `zobrazit` tinyint(1) NOT NULL,
  `nazev` varchar(256) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slider_id` int(10) unsigned NOT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  `poradi` int(10) unsigned NOT NULL,
  `photo_src` varchar(255) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `slide_data`
--

CREATE TABLE IF NOT EXISTS `slide_data` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slide_id` int(10) unsigned NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `zobrazit` tinyint(1) NOT NULL,
  `nazev` varchar(256) NOT NULL,
  `nadpis` varchar(256) NOT NULL,
  `link` varchar(256) NOT NULL,
  `text` text,
  `button_text` varchar(255) NOT NULL,
  `nahled_text` text,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `static_content`
--

CREATE TABLE IF NOT EXISTS `static_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `available_languages` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `static_content_data`
--

CREATE TABLE IF NOT EXISTS `static_content_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `static_content_id` int(11) NOT NULL,
  `popis` text COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  KEY `static_page_id` (`static_content_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `stores`
--

CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `expedition_store` tinyint(1) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `taxes`
--

CREATE TABLE IF NOT EXISTS `taxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `code` varchar(25) CHARACTER SET utf8 NOT NULL,
  `hodnota` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci ROW_FORMAT=DYNAMIC AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(127) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` char(50) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`) USING BTREE,
  UNIQUE KEY `uniq_email` (`email`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `logins`, `last_login`, `created_by`) VALUES
(5, 'info.vzak@gmail.com', 'hana', 'e67fcbdb1f94a7645d31ff55cbdcee5925b5ef1381e5b6f2e4', 305, 1391531214, 5),
(7, 'info@dgstudio.cz', 'dgstudio', '69142eb7316a41f4f72bba0eb52aeb748be1ed742669f98907', 495, 1449566303, 7),
(8, 'tom.barborik@dgstudio.cz', 'tbarborik', 'd0ed154facec00f33b65f4effba4ea171566f1bf8aa5839bf6', 54, 1454367367, 7);

-- --------------------------------------------------------

--
-- Struktura tabulky `user_admin_prefernces`
--

CREATE TABLE IF NOT EXISTS `user_admin_prefernces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `admin_preference_id` int(11) DEFAULT NULL,
  `value` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_log`
--

CREATE TABLE IF NOT EXISTS `user_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `datetime_start` datetime NOT NULL,
  `datetime_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_rights`
--

CREATE TABLE IF NOT EXISTS `user_rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `module_name` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `permission` tinyint(11) NOT NULL DEFAULT '0',
  `readonly` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `module_name` (`module_name`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `user_tokens`
--

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(32) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`) USING BTREE,
  KEY `fk_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `vouchers`
--

CREATE TABLE IF NOT EXISTS `vouchers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `discount_value` decimal(2,0) NOT NULL COMMENT 'sleva % z D0',
  `one_off` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'jednorazovy kupon',
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `used` int(11) NOT NULL DEFAULT '0' COMMENT 'pocet pouziti',
  `lifetime` date DEFAULT NULL,
  `shopper_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabulky `watchdogs`
--

CREATE TABLE IF NOT EXISTS `watchdogs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shopper_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `price` decimal(11,2) NOT NULL,
  `in_stock` tinyint(1) NOT NULL,
  `language_id` int(11) NOT NULL,
  `edited` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
