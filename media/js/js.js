var optionsMil = {
  useEasing : true, 
  useGrouping : true, 
  separator : '', 
  decimal : '.', 
  prefix : '', 
  suffix : 'mil.' 
};


var options = {
  useEasing : true, 
  useGrouping : true, 
  separator : ' ', 
  decimal : '.', 
  prefix : '', 
  suffix : '' 
};
if (!!document.getElementById("countMil")) {
	var mil = new CountUp("countMil", 0, document.getElementById("countMil").innerText.replace("mil.",""), 1, 3, optionsMil); 
	var first = new CountUp("countFirst", 0, document.getElementById("countFirst").innerText.replace(" ",""), 0, 3, options);
	var sec = new CountUp("countSec", 0, document.getElementById("countSec").innerText.replace(" ",""), 0, 3, options);
}
$(window).load(function(){
	
		mil.start();
		first.start();
		sec.start();
});

function slideTo(attr,wtop) {
	$('html, body').animate({
		scrollTop: ($(attr).offset().top - wtop)
	}, 2000);
	$("#sub_menu a").removeClass("actived");
	$("#sub_menu a[href='"+attr.replace("#","/")+"']").addClass("actived");
}

function isScrolledIntoView(elem)
{
	var $elem = $(elem);
	var $window = $(window);

	var docViewTop = $window.scrollTop();
	var docViewBottom = docViewTop + $window.height();

	var elemTop = $elem.offset().top;
	var elemBottom = elemTop + $elem.height();
	
	return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

$(document).ready(function() {
	$('.banky').slick({
		infinite: true,
		prevArrow:'<button type="button" class="slick-prev"></button>',
		nextArrow:'<button type="button" class="slick-next"></button>',
		dots: false,
		slidesToShow: 4,
		slidesToScroll: 1,
	});

	$('#tym-slider').slick({
		prevArrow:'<button type="button" class="slick-prev"></button>',
		nextArrow:'<button type="button" class="slick-next"></button>',
		dots: false,
		infinite: true,
		waitForAnimate: false,

		slidesToShow: 4,
		slidesToScroll: 3,
		responsive: [
		{
	  		breakpoint: 1024,
	  		settings: {
				slidesToShow: 3,
				slidesToScroll: 3,
	  		}
		},
		{
	  		breakpoint: 600,
	  		settings: {
				slidesToShow: 2,
				slidesToScroll: 2
	  		}
		}]
	});
	if($("form").length>0) {
		$("form").submit(function(ev) {
			ev.preventDefault();
			var data = $(this).serialize();
			var form = $(this);
			$.post("?",data,function() {
				$(".information,.info").removeClass("hide");
				$(form).find("input[type='email']").val("");
				$(form).find("input[type='tel']").val("");
				$(form).find("input[type='text']").val("");
				$(form).find("textarea").val("");
				if ($(form).hasClass("newsletter")) {
					$("#newsletterModal").foundation('open');
				}
			});
		});
	}
			
	if ($("#faq-show").length>0) {
		$("#faq-show").click(function(ev) {
			ev.preventDefault();
			
			$(".faq.invisible").addClass("animated slideInUp");
			$(".faq").removeClass("hide");
			$(".faq").removeClass("invisible");
			$(this).addClass("hide");
		}); 
	}
	if ($("#co-rikaji-button").length>0) {
		$("#co-rikaji-button").click(function(ev) {
			ev.preventDefault();
			$("#co_rikaji_backround div.invisible").addClass("animated slideInUp");
			$("#co_rikaji_backround div").removeClass("hide");
			$("#co_rikaji_backround div").removeClass("invisible");
			$(this).addClass("hide");
		}); 
	}
			
	if($("#sub_menu").length>0) {
		$("#sub_menu a").each(function(k,v) {
			if($($(v).attr("href").replace("/","#")).length>0) {
				$(v).click(function(e) {
					e.preventDefault();
					slideTo($(v).attr("href").replace("/","#"),172);

				});
			}
		});
	}
	if(window.location.hash != ""||window.location.hash!="#") {
		slideTo(window.location.hash,10);
	}
});

$(document).scroll(function() {
	if($("#sub_menu").length>0) {
		$($("#sub_menu a").toArray().reverse()).each(function(k,v) {
			if($($(v).attr("href").replace("/","#")).length>0 && isScrolledIntoView($(v).attr("href").replace("/","#"))){
				$("#sub_menu a").removeClass("actived");
				$(v).addClass("actived");
			}
		});
	}
});