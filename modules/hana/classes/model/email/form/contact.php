<?php defined('SYSPATH') or die('No direct script access.');

class Model_Email_Form_Contact extends Model
{
    protected $validate_object;
    protected $form_errors;
    
    public function check($form_data)
    {
        $this->validate_object = Validate::factory($form_data)
            ->filters(TRUE,array('strip_tags' => NULL))
            ->rules('email', array('email'=>NULL,"not_empty"=>NULL))
            ->rule("jmeno","not_empty")
            ->rule("tel","not_empty")
            ->rule("tel","regex",array("/^[+]?[()\/0-9. -]{9,}$/"))
            ->rule("objem","not_empty")
            ->rule("ico", "regex",array("/^[0-9]{8}$/"));


        if($this->validate_object->check()){
            return true;
        }else{
            return false;
        }
    }
    
    public function validate()
    {
       return $this->validate_object;
    }


 
}
?>

