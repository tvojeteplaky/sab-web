<?php defined('SYSPATH') or die('No direct script access.');


class Service_Contact extends Service_Hana_Page
{

    /**
     * Returns headquarter branches with salesmen
     * @param int $language_id
     * @return array
     */
    public static function get_headquarter($language_id = 0)
    {
        return self::get_branches($language_id, true);
    }

    /**
     * Returns all visible branches excluding headquarter
     * @param int $language_id
     * @return array
     */
    public static function get_branches_only($language_id = 0)
    {
        return self::get_branches($language_id);
    }

    public static function get_parts($language_id) {
        $result_data = array();
         $parts_orm = ORM::factory('salesman_part')
            ->language($language_id)
            ->where('zobrazit', '=', 1)
            ->order_by('poradi')
            ->find_all();
            foreach ($parts_orm as $part) {
                $result_data[$part->id] = $part->as_array();
            }
            return $result_data;
    }
    /**
     * Returns branches base on arguments
     * @param int $language_id
     * @param bool|false $headquarter
     * @return array
     */
    private static function get_branches($language_id = 0)
    {
        $return = array();

        $branches = ORM::factory('branch')
            ->language($language_id)
            ->where('zobrazit', '=', 1)
            ->order_by('poradi')
            ->find_all();

        $i = 0;
        foreach ($branches as $branch) {
            $return[$i] = $branch->as_array();
            $return[$i]["photo"] = self::_photo_way_generator($branch->photo_src,"media/photos/branch/item/images-".$branch->id."/",array("ad"=>"jpg","t1"=>"png"));
           
            $i++;
        }

        return $return;
    }

    /**
     * Returns salesmen
     * @param Model_Branch $branch
     * @return array
     */
    public static function get_salesmen(Model_Branch $branch = NULL, $language_id = 0)
    {
        $return = array();

        $salesmen = NULL;
        if (is_null($branch)) {
            $salesmen = ORM::factory("salesman")
                ->language($language_id)
                ->where('zobrazit', '=', 1)
                ->order_by('salesman_type_id')
                ->order_by('poradi')
                ->find_all();
        } else {
            $salesmen = $branch->salesman
                ->where('language_id', '=', $branch->language_id)
                ->where('zobrazit', '=', 1)
                ->order_by('salesman_type_id')
                ->order_by('poradi')
                ->find_all();

        }

        foreach ($salesmen as $salesman)
            $return[] = $salesman->as_array();

        return $return;
    }

    /**
     * Returns salesmen by their type id
     * @param Model_Branch $branch
     * @return array
     */
    public static function get_salesmen_by_type(Model_Branch $branch)
    {
        $return = array();
        $salesmen = $branch->salesman
            ->join('salesman_types')->on('salesmans.salesman_type_id', '=', 'salesman_types.id')
            ->where("zobrazit", "=", 1)
            ->language($branch->language_id)
            ->order_by("salesman_types.poradi")
            ->order_by("poradi")
            ->find_all();

        $i = 0;
        $type = 0;
        foreach ($salesmen as $salesman)
        {
            if ($salesman->salesman_type->id != $type) {
                $i = 0;
                $type = $salesman->salesman_type->id;
                $return[$type]['nazev'] = $salesman->salesman_type->where('language_id', '=', $salesman->language_id)->where('salesman_types.id', '=', $type)->find()->nazev;
            }
            $return[$type][$i] = $salesman->as_array();
            $i++;
        }

        return $return;
    }

    /**
     * Return background for header
     * @param Model_Route $route
     * @return string
     */
    public static function get_header(Model_Route $route)
    {
        $bg_template = new View('header/contact');
        $bg_template->headquarter = self::get_headquarter();
        $page = Service_Page::get_page_by_route_id($route->id);
        $bg_template->photo_src = $page["photo_src"];
        $bg_template->page_category_id = $page["page_category_id"];
        $bg_template->id = $page["id"];
        return $bg_template->render();
    }


    public static function get_data_from_ares($ico)
    {
        $file = @file_get_contents('http://wwwinfo.mfcr.cz/cgi-bin/ares/darv_bas.cgi?ico='.$ico);
        if ($file) $xml = @simplexml_load_string($file);
        $a = array();
        if ($xml) {
         $ns = $xml->getDocNamespaces();
         $data = $xml->children($ns['are']);
         $el = $data->children($ns['D'])->VBAS;
         if (strval($el->ICO) == $ico) {
          $a['ico']     = strval($el->ICO);
          $a['dic']     = strval($el->DIC);
          $a['firma']   = strval($el->OF);
          $a['ulice']   = strval($el->AA->NU).' '.strval($el->AA->CD); //->CO
          $a['mesto']   = strval($el->AA->N);
          $a['psc'] = strval($el->AA->PSC);
          $a['sidlo']   = $a['ulice'].", ".$a['mesto']." ".$a['psc'];
          $a['stav']    = 'ok';
         } else
          $a['stav']    = 'IČ firmy nebylo nalezeno';
         } else
         $a['stav']     = 'Databáze ARES není dostupná';
         
         return $a;
    }
}