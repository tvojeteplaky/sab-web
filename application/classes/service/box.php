<?php defined('SYSPATH') or die('No direct script access.');


class Service_Box
{

	/**
	 * Gets all visibled boxes
	 * @param int $page_id
	 * @param int $language_id
	 * @return array
	 */
	public static function get_box_type_by_page_id($page_id,  $language_id = 0)
	{
		$result_array=array();
		$page_orm = orm::factory("page")
							->where("pages.id","=",$page_id)
							->find();
		$box_type_orm = $page_orm->box_type;

			$result_array = self::get_box_type_by_id($box_type_orm->id,$language_id);
			$result_array["page"] = Service_Page::get_page_by_route_id($page_orm->route_id); 

		return $result_array;
	}

	public static function get_box_type_by_id($box_type_id, $language_id) {
		$result_array = array();
		$box_type =  orm::factory("box_type",$box_type_id);
		$boxes_orm = $box_type->boxes
							->language($language_id)
							->where("zobrazit","=",1)
							->order_by("poradi")
							->find_all();
		 $boxes_array = array();

			foreach ($boxes_orm as $box) {

			  
				$boxes_array[$box->id] = $box->as_array();
				$dirname =  "media/photos/box/item/images-".$box->id."/";
				$boxes_array[$box->id]["photo"] = Service_Page::_photo_way_generator($box->photo_src,$dirname, array("ad"=>"png","at"=>"png","t1"=>"png"));
				$boxes_array[$box->id]["background"] = Service_Page::_photo_way_generator($box->background_src, $dirname, array("ad"=>"png","at"=>"png"));
				
			}
			$result_array = $box_type->as_array();
			$result_array["boxes"]= $boxes_array;


		return $result_array;
	}
}