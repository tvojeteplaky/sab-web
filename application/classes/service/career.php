<?php defined('SYSPATH') or die('No direct script access.');

class Service_Career extends Service_Page
{
	public static function get_careers($language_id) {
		$result_data = array();
		

		$careers = orm::factory("career")
			->language($language_id)
			->where("zobrazit","=",1)
			->find_all();

		foreach ($careers as $career) {
			$result_data[$career->id] = $career->as_array();
		}

		return $result_data;
	}
}