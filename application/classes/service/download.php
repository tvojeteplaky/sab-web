<?php defined('SYSPATH') or die('No direct script access.');


class Service_Download extends Service_Page
{
	public static $photos_resources_dir="media/photos/";
	public static $files_resources_dir="media/files/";
	
	public static function get_all_files($language_id = 0) {
		$result_array = array();
		$downloads_orm = orm::factory("download")
				->language($language_id)
				->where("zobrazit","=",1)

				->find_all();

		foreach ($downloads_orm as $download_orm) {
			$result_array[$download_orm->id] = $download_orm->as_array();
			$result_array[$download_orm->id]["name"] = seo::uprav_fyzicky_nazev($download_orm->nazev);


			$dirname=self::$photos_resources_dir."download/item/images-".$download_orm->id."/";
			$result_array[$download_orm->id]["photo"] = Service_Page::_photo_way_generator($download_orm->photo_src, $dirname, array("ad"=>"png"));


			$files=$download_orm->download_files->where("download_file_data.language_id","=",$download_orm->language_id)->where("download_files.zobrazit","=",1)->order_by("poradi")->find_all();
	   		$filedirname=self::$files_resources_dir."download/item/files-".$download_orm->id."/";
	   		$files_array=array();
	   		$x=1;
	   		foreach($files as $file)
	   		{    
		   		if($file->file_src && file_exists(str_replace('\\', '/',DOCROOT).$filedirname.$file->file_src.".".$file->ext))
		   		{
					$files_array[$x]["file"]=url::base().$filedirname.$file->file_src.".".$file->ext;
					$files_array[$x]["nazev"]=$file->nazev;
					$files_array[$x]["ext"]=$file->ext;
					$files_array[$x]["file_thumb"]=url::base()."media/admin/img/icons/".$file->ext.".png";
					$x++;
				}
			}
	   		$result_array[$download_orm->id]["files"]=$files_array;
		}

		return $result_array;
	}
}