<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Box_Type extends ORM
{

	protected $_has_many = array(
		"boxes" => array(),
		"pages" => array()
	);
}