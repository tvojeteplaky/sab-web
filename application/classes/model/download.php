<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Download extends ORM_Language
{
	public $join_on_routes=false;
	protected $_has_many = array(
		'download_files' => array(),
	);
}