<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Obecna pripojitelna galerie - widget.
 *
 * @author     Pavel Herink
 * @copyright  (c) 2012 Pavel Herink
 */
class Controller_Gallery extends Controller
{

    public function action_default_widget($seo, $settings = "t1-t2")
    {
        $gallery = new View("gallery/default");
        $gallery->photos = $this->_core_functionality($settings);
        $gallery->module = $this->application_context->get_main_controller();
        $this->request->response = $gallery->render();
    }

    public function action_carousel_widget($seo, $settings = "t1-t2")
    {
        $gallery = new View("gallery/carousel");
        $gallery->photos = $this->_core_functionality($settings);
        $gallery->module = $this->application_context->get_main_controller();
        $this->request->response = $gallery->render();
    }

    public function action_matrix_widget($seo, $settings = "t1-t2")
    {
        $gallery = new View("gallery/matrix");
        $gallery->photos = $this->_core_functionality($settings);
        $gallery->module = $this->application_context->get_main_controller();
        $this->request->response = $gallery->render();
    }

    private function _core_functionality($settings = "t1-t2", $special_no = "", $module = "")
    {
        if (!$module) $module = $this->application_context->get_main_controller();
        if ($module == "catalog") $module_db = "product"; else $module_db = $module;

        $service = "Service_" . $module;
        if (class_exists($service)) {
            if (isset($service::$module))
                $module = $service::$module;
            if (isset($service::$module_db))
                $module_db = $service::$module_db;
        }

        $module_db = strtolower($module_db);

        $route_id = $this->application_context->get_route_id();

        $suffixes = explode("-", $settings);
        $detail_suffix = $suffixes[0];
        $thumbnail_suffix = $suffixes[1];

        $preview_suffix = null;
        if (isset($suffixes[2]))
            $preview_suffix = $suffixes[2];
        // automaticky z routy
        $id = DB::select(array($module_db . "_data." . $module_db . "_id", "id"))->from($module_db . "_data")->where("route_id", "=", $route_id)->execute()->get('id');

        $sub_module = "item";
        if ($module == "page") {
            $page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
            if ($page["page_category_id"] == 2)
                $sub_module = "unrelated";
        }

        $photos = orm::factory($module_db . "_p" . $special_no . "hoto")->where($module_db . "_id", "=", $id)->where("zobrazit", "=", 1)->order_by("poradi", "asc")->language(0)->find_all();;

        $photodir = "media/photos/" . $module . "/{$sub_module}/gallery" . $special_no . "/images-" . $id . "/";

        $photos_array = array();

        $i = 0;
        foreach ($photos as $photo) {
            if ($photo->photo_src) {
                if (file_exists(str_replace('\\', '/', DOCROOT) . $photodir . $photo->photo_src . "-" . $detail_suffix . '.' . $photo->ext)) {
                    $photos_array[$i] = $photo->as_array();
                    $photos_array[$i]["photo"] = url::base() . $photodir . $photo->photo_src . "-" . $thumbnail_suffix . '.' . $photo->ext;
                    $photos_array[$i]["photo_detail"] = url::base() . $photodir . $photo->photo_src . "-" . $detail_suffix . '.' . $photo->ext;
                    $photos_array[$i]["photo_preview"] = url::base() . $photodir . $photo->photo_src . "-" . $preview_suffix . '.' . $photo->ext;
                    $photos_array[$i]["nazev"] = $photo->nazev;
                    $photos_array[$i]["gallery_id"] = $id;
                    $photos_array[$i]["photo_path"] = url::base() . $photodir . $photo->photo_src . "-";
                    $i++;
                }
            }
        }

        return ($photos_array);
    }


}

?>