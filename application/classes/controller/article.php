<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Generovani statickych obsahovych stranek a textu.
 */
class Controller_Article extends Controller
{

    /**
     * Metoda generujici seznam clanku.
     */
    public function action_index($page = NULL)
    {
        if ($page == NULL)
            $page = 1;

        $template = new View("article/list");
        $language_id = $this->application_context->get_actual_language_id();
        $route_id = $this->application_context->get_route_id();
        $page_orm = Service_Page::get_page_by_route_id($route_id);

        //die(print_r($page));
        $template->item = $page_orm;
        $template->news = Service_Article::get_article_list($language_id,0,3);
         $items_per_page = 6;
         $pagination = Pagination::factory(array(
             'current_page' => array('source' => $this->application_context->get_actual_seo(), 'value' => $page),
             'total_items' => Service_Article::get_article_total_items_list($language_id, 0,$template->news),
             'items_per_page' => $items_per_page,
             'view' => 'pagination/basic',
             'auto_hide' => TRUE
         ));
        $template->prefered = Service_Article::get_article_list($language_id,0,$pagination->items_per_page,$pagination->offset,$template->news);

         $template->pagination = $pagination->render();
        $this->request->response = $template->render();
    }

    public function action_category($page = NULL)
    {
         if ($page == NULL)
            $page = 1;

        $language_id = $this->application_context->get_actual_language_id();

        $template = new View('article/list');
        
        $category = Service_Article::get_article_category_by_route_id($this->application_context->get_route_id());
        $template->item=$category;
        $template->news = Service_Article::get_article_list($language_id,$category["id"],3);
        $items_per_page = 6;
        $pagination = Pagination::factory(array(
             'current_page' => array('source' => $this->application_context->get_actual_seo(), 'value' => $page),
             'total_items' => Service_Article::get_article_total_items_list($language_id, $category["id"],$template->news),
             'items_per_page' => $items_per_page,
             'view' => 'pagination/basic',
             'auto_hide' => TRUE
         ));

        $template->prefered = Service_Article::get_article_list($language_id,$category["id"],$pagination->items_per_page,$pagination->offset,$template->news);

        $template->pagination = $pagination->render();
        $this->request->response = $template->render();
    }

    /**
     * Metoda generujici seznam clanku na uvodce.
     */
    public function action_widget($seo, $category_id = null)
    {
        $template = new View("article/widget");
        $language_id = $this->application_context->get_actual_language_id();
        $template->items = Service_Article::get_article_list($language_id, 0, 2);
        $this->request->response = $template->render();
    }

    /**
     * Metoda generujici seznam clanku - uvodka.
     */
    public function action_homepage_list()
    {
        $template = new View("article/widget");
        $language_id = $this->application_context->get_actual_language_id();
        $template->items = Service_Article::get_article_list($language_id, 0, 3);
        $this->request->response = $template->render();
    }


    /**
     * Metoda generujici vsechny stranky vkladane do hlavniho obsahu.
     */
    public function action_detail()
    {
        $route_id = $this->application_context->get_route_id();
        $template = new View("article/detail");
        $sel_links = Hana_Navigation::instance()->get_navigation_breadcrumbs();
        $template->item = Service_Article::get_article_by_route_id($route_id);
        $template->prev = current($sel_links);
        //$template->prev = current($sel_links);
        $this->request->response = $template->render();
    }

    public function action_subnav()
    {
        $subnav = new View("navigation/subnav");
        $subnav->items = Service_Article::get_article_category_list($this->application_context->get_actual_language_id());
        $subnav->sel_links =Hana_Navigation::instance()->get_navigation_breadcrumbs();
        $this->request->response = $subnav->render();
    }


}

?>