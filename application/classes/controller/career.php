<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Career extends Controller
{

	public function action_index()
	{
		$template = new View('career/detail');
		$template->page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
		$template->careers = Service_Career::get_careers($this->application_context->get_actual_language_id());
		$this->request->response = $template->render();
	}


	public function action_subnav($nazev_seo)
	{
		$subnav = new View("navigation/subnav");
		$page = Service_Page::get_page_by_route_id($this->application_context->get_actual_route());
		$links = Service_Page::get_pages_with_parent(Service_Page::get_top_parent_page($page),1,1,1);

		$breadcrumbs = Hana_Navigation::instance()->get_navigation_breadcrumbs();

		$subnav->items = $links;
		$subnav->breadcrumbs = $breadcrumbs;
		$subnav->parent = array_pop($breadcrumbs);
		$this->request->response = $subnav->render();
	}
}