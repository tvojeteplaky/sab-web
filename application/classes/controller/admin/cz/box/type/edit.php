<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Box_Type_Edit extends Controller_Hana_Edit
{

	public function before() {
		$this->orm = new Model_Box_Type();
		parent::before();
	}

	protected function _column_definitions()
	{
		$this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
		$this->auto_edit_table->row("nazev")->type("edit")->label("Název")->set();
		$this->auto_edit_table->row("settings")->type("edit")->label("Nastavení")->condition("[link,link_nazev,rok,main_img(png),background_img(jpg),popis]")->set();
		$this->auto_edit_table->row("template")->type("edit")->label("Šablona")->set();
		$this->auto_edit_table->row("zobrazit")->type("checkbox")->label("Zobrazit")->default_value(1)->set();

		
	}

}