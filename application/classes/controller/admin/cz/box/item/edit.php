<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Box_Item_Edit extends Controller_Hana_Edit
{

	public function before() {
		$this->orm = new Model_Box();
		parent::before();
	}

	protected function _column_definitions()
	{
		$this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
		$this->auto_edit_table->row("nazev")->type("edit")->label("Název")->set();
		$this->auto_edit_table->row("box_type_id")->type("selectbox")->data_src( array("related_table_1" => "box_type","orm_tree"=>false))->label("Typ")->set();
		$this->auto_edit_table->row("zobrazit")->type("checkbox")->label("Zobrazit")->default_value(1)->set();
		if($this->orm->box_type_id){
			$settings = explode(",",$this->orm->box_type->settings);

			if($settings[0]>0) $this->auto_edit_table->row("link")->type("edit")->label("Odkaz")->set();
			if($settings[1]>0 || is_string($settings[2])) $this->auto_edit_table->row("link_nazev")->type("edit")->label((is_string($settings[1])?$settings[1]:"Text odkazu"))->set();
			if($settings[2]>0) $this->auto_edit_table->row("link_nazev")->type("edit")->label("Rok")->set();
			if($settings[3]>0) {
				$this->auto_edit_table->row("main_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
				$this->auto_edit_table->row("main_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at", "ext"=>"png", "delete_link"=>true))->label("Náhled obrázku")->set();
			}
			if($settings[4]>0) {
			$this->auto_edit_table->row("background_image_src")->type("filebrowser")->label("Pozadí boxu")->set();
			$this->auto_edit_table->row("background_image")->type("image")->item_settings(array("db_col_name"=>"background_src","dir"=>$this->subject_dir,"suffix"=>"at","ext"=>"png","delete_link"=>true))->label("Náhled obrázku")->set();
			}
			if($settings[5]>0) {
				$this->auto_edit_table->row("popis")->type("editor")->label("Text")->set();
			}
		}
	}


	protected function _form_action_main_postvalidate($data) {
		parent::_form_action_main_postvalidate($data);


		if(isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"])
		{
			// nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
			$image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
			$this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($data['nazev']), true, 'png');
		}

		if(isset($_FILES["background_image_src"]) && $_FILES["background_image_src"]["name"])
		{
			// nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
			$image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "background");
			$this->module_service->insert_image("background_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($data['nazev']).'_background',true,'png','background_src');
		}
	}

	protected function _form_action_main_image_delete($data)
	{
		$this->module_service->delete_image($data["delete_image_id"], $this->subject_dir, false, false, false, 'photo_src', 'ext', false, 'photo', 'png', $this);
	}

	protected function _form_action_background_image_delete($data)
	{
		$this->module_service->delete_image($data["delete_image_id"], $this->subject_dir, false, false, false, 'background_src', 'ext', false, 'background', 'png', $this);
	}
}