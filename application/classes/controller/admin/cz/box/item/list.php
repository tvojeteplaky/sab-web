<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Box_Item_List extends Controller_Hana_List
{

     protected $default_order_by = "poradi";
    protected $default_order_direction= "asc";

    public function before() {
        $this->orm=new Model_Box();
        parent::before();
    }


    protected function _column_definitions()
    {
        $this->auto_list_table->column("id")->label("# ID")->width(50)->set();

        $this->auto_list_table->column("nazev")->type("link")->label("Název")->item_settings(array("hrefid"=>$this->base_path_to_edit))->css_class("txtLeft")->filterable()->sequenceable()->width(250)->set();
            $this->auto_list_table->column("box_type_id")->label("Typ")->data_src(array("related_table_1" => "box_type", "column_name" => "nazev"))->sequenceable("box_types.nazev")->width(500)->filterable(array("col_name" => "box_types.id"))->set();

        if(Kohana::config("languages")->get("enabled"))
            $this->auto_list_table->column("available_languages")->type("languages")->item_settings(array("hrefid"=>$this->base_path_to_edit))->width(60)->set();
        $this->auto_list_table->column("poradi")->type("changeOrderShifts")->label("")->sequenceable()->width(32)->exportable(false)->printable(false)->set();
        $this->auto_list_table->column("zobrazit")->type("switch")->item_settings(array("action"=>"change_visibility","states"=>array(0=>array("image"=>"lightbulb_off.png","label"=>"neaktivní"),1=>array("image"=>"lightbulb.png","label"=>"aktivní"))))->sequenceable()->filterable()->label("")->width(32)->set();
        $this->auto_list_table->column("delete")->type("checkbox")->value(0)->label("")->width(30)->exportable(false)->printable(false)->set();
    }

    protected function _orm_setup()
    {
         $this->orm->join("box_types", "left")->on("boxes.box_type_id", "=", "box_types.id");
    }
}