<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Download_Item_Edit extends Controller_Hana_Edit
{	
	protected $subject;
	protected $subject_col_id_name;
	protected $subject_files_name;

	public function before() {
		$this->orm = new Model_Download();
		parent::before();

		$this->subject = strtolower($this->orm->class_name);
		$this->subject_col_id_name = $this->subject . "_id";
		$this->subject_files_name = $this->subject . "_file";
	}

	protected function _column_definitions()
	{
		$this->auto_edit_table->row("id")->item_settings(array("with_hidden"=>true))->label("# ID")->set();
		$this->auto_edit_table->row("nazev")->type("edit")->label("Název")->set();
		$this->auto_edit_table->row("zobrazit")->type("checkbox")->label("Zobrazit")->default_value(1)->set();

		$this->auto_edit_table->row("main_image_src")->type("filebrowser")->label("Zdroj obrázku")->set();
		$this->auto_edit_table->row("main_image")->type("image")->item_settings(array("dir"=>$this->subject_dir,"suffix"=>"at", "ext"=>"png", "delete_link"=>true))->label("Náhled obrázku")->set();

		//Soubory připojené k produktu
				$this->auto_edit_table->row("L2")->variant("one_col")->value("Soubory připojené k produktu")->type("label")->set();
				if ($this->orm->id) {
					$this->auto_edit_table->row("download_file")->item_settings(array("orm" => $this->orm, "title" => "Soubor", "item" => "nazev", "description" => "", "value" => array("download_file_data.nazev", "phodnota"), "files_order_by" => array("poradi" => "desc")))->value("Seznam souborů")->type("microeditfile")->set();
				} else {
					$this->auto_edit_table->row("L12")->value("Před přidáváním souborů musí být produkt nejprve uložen")->type("text")->set();
				}
	}


	protected function _form_action_main_postvalidate($data) {
		parent::_form_action_main_postvalidate($data);


		if(isset($_FILES["main_image_src"]) && $_FILES["main_image_src"]["name"])
		{
			// nahraju si z tabulky settings konfiguracni nastaveni pro obrazky - tzn. prefixy obrazku a jejich nastaveni
			$image_settings = Service_Hana_Setting::instance()->get_sequence_array($this->module_key, $this->submodule_key, "photo");
			$this->module_service->insert_image("main_image_src", $this->subject_dir, $image_settings, seo::uprav_fyzicky_nazev($data['nazev']), true, 'png');
		}


	}

	protected function _form_action_main_image_delete($data)
	{
		$this->module_service->delete_image($data["delete_image_id"], $this->subject_dir, false, false, false, 'photo_src', 'ext', false, 'photo', 'png', $this);
	}

	protected function _form_action_microedit_download_file_add($data)
	{
		$errors = "";
		// primitivni predvalidovani dat
		//if(!$data["nazev"]) $errors["nazev"]="musí být zadán název";
		if (!$data["file_id"]) {
			// prvni zadavani - musi byt zdroj obrazku a pokud neni nazev, vytvori se z tohoto zdroje
			if (!$_FILES['microedit_file_src']["name"]) $errors["src"] = "musí být vybrán soubor obrázku";
			if (empty($errors) && !$data["nazev"]) $data["nazev"] = Service_Hana_File::get_raw_file_name($_FILES['microedit_file_src']['name']);
		} else {
			// editace - nemusi byt zdroj obrazku

			if (!$_FILES['microedit_file_src']["name"]) {
				// 1) neni obrazek - musi byt nazev
				if (!$data["nazev"]) $errors["nazev"] = "název obrázku musí být zadán";
			} else {
				// 2) je obrazek - pokud neni nazev - pouziju opet nazev z obrazku
				if (!$data["nazev"]) $data["nazev"] = Service_Hana_File::get_raw_file_name($_FILES['microedit_file_src']['name']);
			}

		}
		// validace probehly, data jsou predzpracovana, prejdu k procesu jejich ulozeni

		// vlastni zpracovani dat
		if (empty($errors)) {

			// ziskani vychoziho ormka v zavislosti na tom, zda jde o editaci nebo novou polozku
			if ($data["file_id"]) {
				$subject_files = orm::factory($this->subject_files_name)->language(1)->where($this->subject . "_files.id", "=", $data["file_id"])->find();

			} else {
				$subject_files = orm::factory($this->subject_files_name)->language(1);
			}

			// predam data ke zpracovani a ulozeni modulove servise
			$result = $this->module_service->insert_file($subject_files, $this->subject_col_id_name, $this->item_id, "microedit_file_src", $this->subject_dir, $data);
		}

		// pokud servisa vrati chybu, pridam ji do chyb
		if (isset($result) && $result !== true) $errors = $result;

		// zhodnoceni vysledku
		if ($errors) {
			// neuspesne zpracovani dat
			$this->data_processing_errors = $errors; // nastaveni chyby
			$data["action"] = "main_gallery_editphoto"; // toto nastaveni zpusobi, ze se otevre znovu dialogove okno
			return ($data);

		} else {
			// uspesne zpracovani dat
			$this->data_saved = true;
		}
	}

	protected function _form_action_microedit_download_file_delete($data)
	{
		$orm = orm::factory($this->subject_files_name);
		$this->module_service->delete_file($data["delete_file_id"], $this->subject_dir, $orm, $this->subject_col_id_name);
		$this->data_saved = true;
	}

}