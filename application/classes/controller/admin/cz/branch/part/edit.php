<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_Cz_Branch_Part_Edit extends Controller_Hana_Edit
{

    public function before()
    {
        $this->orm = new Model_Salesman_Part();
        parent::before();
    }

    protected function _column_definitions()
    {
        $this->auto_edit_table->row("id")->item_settings(array("with_hidden" => true))->label("# ID")->set();
        $this->auto_edit_table->row("nazev")->type("edit")->label("Název")->set();
         $this->auto_edit_table->row("popis")->type("editor")->label("Text")->set();
        $this->auto_edit_table->row("zobrazit")->type("checkbox")->label("Zobrazit")->default_value(1)->set();

    }


}