<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Box extends Controller
{

	public function action_widget()
	{
		$template = new View('box/widget');
		$template->boxes = Service_Box::get_boxes();
		$this->request->response = $template->render();
	}

	public function action_box_generator($nazev_seo,$box_type_id=0) {
		$box_type = Service_Box::get_box_type_by_id($box_type_id,$this->application_context->get_actual_language_id());

		$template = new View("box/".$box_type["template"]);
		$template->boxes = $box_type["boxes"];
		$template->nazev_seo = $nazev_seo;

		$this->request->response = $template->render();
	}

	public function action_structured_box($nazev_seo,$page_id=0) {
		$box_type = Service_Box::get_box_type_by_page_id($page_id,$this->application_context->get_actual_language_id());
		if (!empty($box_type["template"])) {
			$template = new View("box/".$box_type["template"]);
			$template->boxes = $box_type["boxes"];
			$template->page = $box_type["page"];
			$template->nazev_seo = $nazev_seo;
			$this->request->response = $template->render();
		} else {
			$this->request->response = "";
		}

		
	}

}