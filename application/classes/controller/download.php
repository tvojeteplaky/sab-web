<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Download extends Controller
{
	/**
	 * Zobrazi stránku s kontakty
	 */
	public function action_index()
	{
		$route_id = $this->application_context->get_route_id();
		$template = new View("download/list");
		$template->item = Service_Page::get_page_by_route_id($route_id);
		$template->downloads= Service_Download::get_all_files($this->application_context->get_actual_language_id());
		$this->request->response = $template->render();
	}

	public function action_subnav() {
		
	}

}