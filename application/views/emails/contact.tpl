<table cellpadding="5" style="border: none; border-collapse: collapse;">
    <tr>
        <td><strong>Zpráva z kontaktního formuláře www stránek</strong></td>
        <td>{$data.nazev_projektu}</td>
    </tr>
    <tr>
        <td><strong>Název stránky</strong></td>
        <td>{$data.nazev_stranky}</td>
    </tr>
    <tr>
        <td><strong>URL</strong></td>
        <td>{$data.url}</td>
    </tr>
    {if !empty($data.jmeno)}
    <tr>
        <td><strong>Jméno a příjmení</strong></td>
        <td>{$data.jmeno}</td>
    </tr>
    {/if}
    {if !empty($data.ico)}
    <tr>
        <td><strong>IČO</strong></td>
        <td>{$data.ico}</td>
    </tr>
    {/if}
    {if !empty($data.email)}
    <tr>
        <td><strong>E-mail odesílatele</strong></td>
        <td>{$data.email}</td>
    </tr>
    {/if}
    {if !empty($data.tel)}
        <tr>
            <td><strong>Telefon</strong></td>
            <td>{$data.tel}</td>
        </tr>
    {/if}
    <tr>
        <td><strong>Typ klienta</strong></td>
        <td>{$data.typ_klienta}</td>
    </tr>
    {if !empty($data.place)}
    <tr>
        <td><strong>Místo schůzky</strong></td>
        <td>{$data.place}</td>
    </tr>
    {/if}
        {if !empty($data.objem)}
    <tr>
        <td><strong>Měsíční objem</strong></td>
        <td>{$data.objem}</td>
    </tr>
    {/if}
    {if !empty($data.ucel)}
    <tr>
        <td><strong>Účel směny</strong></td>
        <td>{$data.ucel}</td>
    </tr>
    {/if}
     {if !empty($data.cetnost)}
    <tr>
        <td><strong>Četnost směny</strong></td>
        <td>{$data.cetnost}</td>
    </tr>
    {/if}
</table>