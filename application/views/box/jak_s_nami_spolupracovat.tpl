<section id="jakSpolupracovat">
		<div class="row">
			<div class="small-12 columns">
				<h2 class="title_decor wow slideInLeft" id="{$page.nazev_seo}">{$page.nadpis}</h2>
			</div>
		</div>
		{foreach from=$boxes item=box key=key name=jak_spoluprac}
		{capture assign="texty" }
			<h3>
						{$box.nazev}
					
					</h3>
					<p>
						{$box.popis}
					
					</p>
		{/capture}
		{capture assign="photo" }
			<img src="{$box.photo.ad}" alt="{$box.nazev}" title="{$box.nazev}" /> 
		{/capture}
			{if $smarty.foreach.jak_spoluprac.index eq 0}
			<div class="row a wow slideInLeft">		
		
				<div class="large-11 large-offset-1 small-12 columns end">
					<div class="row">
						<div class="large-2 medium-2 columns">
							{$photo}
						
						</div>
						<div class="large-10 medium-10 columns">
							{$texty}
						</div>
					</div>
				</div>
			</div>
			{elseif $smarty.foreach.jak_spoluprac.index eq 1}
			<div class="row b wow slideInLeft">		
				<div class="large-3 large-offset-2 medium-4 hide-for-small-only columns">
		    			<img src="{$media_path}img/line1.png" alt="Čára">
		    		</div>
		    		<div class="large-6 end medium-8 columns">
		    			<div class="row">
	    				<div class="large-8 medium-9 columns text-right">
	    					{$texty}
	    				</div>
	    				<div class="large-4 medium-3 columns">
	    					{$photo}
	    				</div>
	    			</div>
		    		</div>
			</div>
			{elseif $smarty.foreach.jak_spoluprac.index eq 2 || ($smarty.foreach.jak_spoluprac.index>3&&$smarty.foreach.jak_spoluprac.index%2==0)}
			<div class="row c wow slideInLeft">		
				<div class="large-6 large-offset-1 medium-8 columns">
		    			<div class="row">
		    				<div class="large-3 medium-3 columns">
		    					{$photo}
		    				</div>
		    				<div class="large-9 medium-9 columns">
		    					{$texty}
		    				</div>
		    			</div>
		    		</div>
		    		<div class="large-4 end medium-4 hide-for-small-only columns">
		    			<img src="/media/img/line2.png" alt="Čára">
		    		</div>
			</div>	
			{elseif $smarty.foreach.jak_spoluprac.index eq 3 || ($smarty.foreach.jak_spoluprac.index>3&&$smarty.foreach.jak_spoluprac.index%2!=0)}
			<div class="row d wow slideInLeft">
		    		<div class="large-3 large-offset-1 medium-4 hide-for-small-only columns">
		    			<img src="/media/img/line3.png" alt="Čára">
		    		</div>
		    		<div class="large-8 medium-8 columns">
		    			<div class="row">
		    				<div class="large-8 medium-9 columns text-right">
		    					{$texty}
		    				</div>
		    				<div class="large-3 end medium-3 columns">
		    					{$photo}
		    				</div>
		    			</div>
		    		</div>
		    	</div>	
			{/if}
				
		{/foreach}
		
		<div class="row center wow slideInLeft">
			<a href="#" class="button" data-open="zajem">Mám zájem</a>
		</div>
	</section>