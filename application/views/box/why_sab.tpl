<div class="whySAB ">
        <div class="row" style="margin-top: 0;">
          <div class="small-12 columns">
            <h2 class="wow slideInLeft"><b>Proč</b> SAB Finance a.s.?</h2>
          </div>
        </div>
        <div class="links row small-up-1 medium-up-2 large-up-3 wow slideInLeft">
        {foreach from=$boxes item=box key=key name=name}
          <div class="column">
            <a href="{$box.link}">
              <div>
                <img src="{$box.background.ad}" alt="{$box.nazev}">
                <img src="{$box.photo.ad}" alt="{$box.nazev}">
                {$box.popis}
              </div>
            </a>
          </div>
        {/foreach}
        </div>
        <div class="row">
          <div class="small-12 small-centered columns text-center">
            <a href="/pro-klienty#proc-spolupracovat-s-nami" class="button secondary wow slideInLeft">Více o spolupráci</a>
          </div>
        </div>
      </div>