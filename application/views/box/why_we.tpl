{if !empty($boxes)}
    

<section style="float: left; width: 100%;" class="slovakFix procSpolupracovat">
        <div class="row title_box wow slideInLeft">
            <div class="small-12 columns">
                <h2 class="title_decor" id="{$page.nazev_seo}">{$page.nadpis}</h2>
            </div>
        </div>

        {*{foreach from=$boxes item=box key=key name=why_we}
        {if $smarty.foreach.why_we.first || $smarty.foreach.why_we.index%3 == 0}
            <div class="row wow slideInLeft">
        {/if}
            <article class="part_33">
                <div class="part_20">
                    <img src="{$box.photo.ad}" alt="{$box.nazev}" title="{$box.nazev}" class="item_pro_klienta_icons" />
                </div>
                <div class="part_80">
                    <h3>
                        {$box.nazev}
                    </h3>
                       {$box.popis}

                </div>
            </article>
            {if $smarty.foreach.why_we.last || $smarty.foreach.why_we.iteration%3 == 0}
            </div>
            {/if}
        {/foreach}*}

    <div class="row wow slideInLeft">
        <div class="small-up-1 medium-up-2 large-up-3">
            {foreach from=$boxes item=box key=key name=why_we}
                <div class="column">
                    <div class="row">
                        <div class="small-3 columns">
                            <img src="{$box.photo.ad}" alt="{$box.nazev}" title="{$box.nazev}" class="item_pro_klienta_icons">
                        </div>
                        <div class="small-9 columns">
                            <h3>
                                {$box.nazev}
                            </h3>
                            <p>
                                {$box.popis}
                            </p>
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
    </section>
    {/if}