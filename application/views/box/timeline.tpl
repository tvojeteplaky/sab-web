<section style="float: left; width: 100%;" class="slovakFix">
		<div class="row title_box timeLineHead" style="padding-bottom: 20px;">
	
			<div class="small-12 columns">
				<h2 class="title_decor" id="{$page.nazev_seo}">{$page.nadpis}</h2>
			</div>
	
		</div>
		<!--<script type="text/javascript" src="{$media_path}js/timeline.js"></script>-->
		<div class="row hide-for-small-only">
			<div class="small-12 columns text-center">

					<button  type="button" class="timeline-button timeline-up button">Novější</button>
			</div>
		</div>
		
		<div class="timeline" id="timeline">
			<div class="row hide-for-small-only" id="scrollTimeline">
				<div class="small-6 columns left year">
					{foreach from=$boxes item=box key=key name=left_timeline}
						{if $smarty.foreach.left_timeline.index%2 == 0}		
						<div class="item" data-year="{$box.link_nazev}">
							<div class="container">
								<div class="content-wrapper">
									<span>{$box.link_nazev}</span>
									<h3>{$box.nazev}</h3>
									<p>{$box.popis}
									</p>
								</div>
							</div>
						</div>
						{/if}
					{/foreach}
				</div>
				<div class="small-6 columns right year">
					{foreach from=$boxes item=box key=key name=left_timeline}
						{if $smarty.foreach.left_timeline.iteration%2 == 0}		
						<div class="item" data-year="{$box.link_nazev}">
							<div class="container">
								<div class="content-wrapper">
									<span>{$box.link_nazev}</span>
									<h3>{$box.nazev}</h3>
									<p>{$box.popis}
									</p>
								</div>
							</div>
						</div>
						{/if}
					{/foreach}
				</div>
			</div>

			<div class="show-for-small-only">
				{foreach from=$boxes item=box key=key name=left_timeline}
					<div class="row">
						<div class="small-12 columns">
							<div class="item" data-year="{$box.link_nazev}">
								<div class="container">
									<div class="content-wrapper">
										<h2>{$box.link_nazev}</h2>
										<h3>{$box.nazev}</h3>
										<p>{$box.popis}
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				{/foreach}
			</div>
	
		</div>
		<div class="row hide-for-small-only">
			<div class="small-12 columns text-center">

					<button  type="button" class="timeline-button timeline-down button">Starší</button>

			</div>
		</div>
	</section>