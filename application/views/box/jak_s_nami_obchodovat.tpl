<section class="whySAB">
    
        <div class="row">
    
          <div class="small-12 columns">
    
            <h2><span class="bold_text  wow slideInLeft">Jak</span> s námi obchodovat?</h2>
    
          </div>
    
        </div>
    
        <div class="row small-up-1 medium-up-3 large-up-3 obchodovat_boxes  wow slideInLeft">
        {foreach from=$boxes item=box key=key name=jak_obchodovat}
          <div class="column">
    
            <div class="row line_blue_{if ($smarty.foreach.jak_obchodovat.first)}left{elseif ($smarty.foreach.jak_obchodovat.last)}right{else}center{/if}">
    
              <img src="{$box.photo.ad}" alt="{$box.nazev}" title="{$box.nazev}" class="obchodovat_item_ico" />
    
            </div>
    
            <div class="row">
    
              <h3 class="classical_padd">
    
                {$box.nazev}
    
              </h3>
    
              <p class="classical_padd">
    
                {$box.popis}
              </p>
    
            </div>
    
          </div>
        {/foreach}
    
        </div>
    
        <div class="row">
    
          <div class="small-12 small-centered columns text-center">
            <a href="/pro-klienty#co-nabizime" class="button secondary  wow slideInLeft">Detailní popis</a>
          </div>
    
        </div>
    
      </section>