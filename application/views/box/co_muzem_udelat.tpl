    <section class="whySAB" id="udelame_sector">
    
        <div class="row">
    
          <div class="small-12 columns">
    
            <h2><span class="bold_text  wow slideInLeft">Co</span> pro Vás můžeme udělat?</h2>
    
          </div>
    
        </div>
    
        <div class="row small-up-1 medium-up-3 large-up-6 udelame_boxes  wow slideInLeft">
    {foreach from=$boxes item=box key=key name=name}
      <div class="column">
      <a href="{$box.link}">
            <img src="{$box.photo.ad}" alt="{$box.nazev}" title="{$box.nazev}" class="udelame_item_ico" />
    
            <p class="bold_text">
    
              {$box.popis}
    
            </p>
          </a>
          </div>
    {/foreach}
    
        </div>
      <div class="row">
          <div class="small-12 small-centered columns text-center">
            <a href="/pro-klienty#co-pro-vas-muzeme-udelat" class="button secondary wow slideInLeft">Naše služby</a>
          </div>
        </div>
      </section>