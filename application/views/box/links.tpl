<section class="o-spolecnosti_boxy">
		<div class="row">
		{foreach from=$boxes item=box key=key name=name}
			<div class="large-4 columns">
				<div>
					<strong>{$box.nazev}</strong>
					<br>
					{$box.popis}
					<br>
					<a href="{$box.link}" class="button">{$box.link_nazev}</a>
				</div>
			</div>
		{/foreach}
		</div>
	</section>