<section>
		<div class="row title_box">
			<div class="small-12 columns">
				<h2 class="title_decor wow slideInLeft" id="{$page.nazev_seo}">
					{$page.nadpis}
				</h2>
			</div>
		</div>
		<div class="row">
			<div class="small-12 columns">
			<ul class="accordion faq wow slideInLeft" data-accordion data-allow-all-closed="true">
				{foreach from=$boxes item=box key=key name=faq}
				
					<li class="accordion-item{if $smarty.foreach.faq.index >= 4} invisible hide{/if}" data-accordion-item>
						<a href="#" class="accordion-title">{$box.nazev}</a>
						<div class="accordion-content" data-tab-content>
							{$box.popis}
						</div>
					</li>
				{/foreach}
			</ul>
			</div>
		</div>


			
		<div class="row center wow slideInLeft" style="margin-top: 60px;">
			<a href="#" class="button" id="faq-show">Další dotazy</a>
		</div>
	</section>