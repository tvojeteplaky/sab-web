<section id="banking">
		<div class="row">
			<div class="small-12 columns">
				<h2 class="title_decor wow slideInLeft" id="bankovni-spojeni">{$page.nadpis}</h2>
			</div>
		</div>
		{$page.uvodni_popis}
		
		<div class="row wow slideInLeft hide-for-small-only" id="banks" style="text-align: center;">
			<ul class="banky" data-equalizer>
                                        {foreach from=$boxes item=box key=key name=banky}
                                            <li data-equalizer-watch><div>{$box.nazev}</div></li>
                                        {/foreach}
			</ul>
		</div>
		<div class="show-for-small-only row small-up-1">
			{foreach from=$boxes item=box key=key name=banky}
				<div class="column"><div>{$box.nazev}</div></div>
			{/foreach}
			<div class="column">
				<br>
			</div>
		</div>
		<div class="row spodek wow slideInLeft">
    			<div class="small-12 columns">
    				{$page.popis}
    			</div>
    		</div>
	</section>