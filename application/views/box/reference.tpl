       {if ($nazev_seo!="index")}
    <section class="sliderik" id="co_rikaji_backround" style="margin-top: 0;">
    <div class="row wow slideInLeft">
      <h2 class="sliderik_title  wow slideInLeft" id="{$page.nazev_seo}">{$page.nadpis|bbcode}</h2>

    </div>
       {/if}
      {foreach from=$boxes item=box key=key name=reference}
        {if $smarty.foreach.reference.first || $smarty.foreach.reference.index%3 == 0 }
          <div class="row small-up-1 medium-up-3 large-up-3 {if $smarty.foreach.reference.first}wow slideInLeft{else}invisible{/if}" data-equalizer>
        {/if}
          <div class="column">
    
            <p  class="justify" data-equalizer-watch>
    
              {$box.popis|strip_tags:false}												
              <br /><br />
              {if !empty($box.link_nazev)}<strong>{$box.link_nazev}</strong><br />{/if}
              <strong>{$box.nazev}</strong>
              {if !empty($box.photo.ad)}<br /><br /><img src="{$box.photo.ad}" style="max-height: 3rem; max-width: 15rem; ">{/if}
            </p>
    
          </div>
        {if $smarty.foreach.reference.last || $smarty.foreach.reference.iteration%3 == 0}
          </div>
        {/if}
      {/foreach}
      
      {if count($boxes)>3}
        
      
        
        <div class="row">
    
          <div class="row center" style="margin-top: 30px;">
    
            <a href="pro-klienty#co-o-nas-rikaji-nasi-klienti" class="button wow slideInLeft" id="co-rikaji-button">Více referencí</a>
    
          </div>
    
        </div>
        {/if}

               {if ($nazev_seo!="index")}

               </section>
               {/if}