<section class="legislativa {$page.nazev_seo}-class">
		<div class="row title_box  wow slideInLeft">
			<div class="small-12 columns">
				<h2 class="title_decor" id="{$page.nazev_seo}">{$page.nadpis}</h2>
			</div>
		</div>
		{*<div class="row wow slideInLeft">
		  <div class="sector">
		  {foreach from=$boxes item=box key=key name=we_offer}
		  	<article class="part_33">
				<div class="udelame_box" id="udelame_box_{$smarty.foreach.we_offer.iteration}" style="background-image: url('{$box.background.ad}');">
					<h3>
						{$box.nazev}
					</h3>

					
						{$box.popis}
					
				</div>
			</article>
		  {/foreach}
		</div>
	  </div>*}

		<div class="row small-up-1 medium-up-2 large-up-3 wow slideInLeft">
			{foreach from=$boxes item=box key=key name=we_offer}
				<div class="column">
					<div class="udelame_box" id="udelame_box_{$smarty.foreach.we_offer.iteration}" style="background-image: url('{$box.background.ad}');">
						<h3>
							{$box.nazev}
						</h3>


						{$box.popis}

					</div>
				</div>
			{/foreach}
		</div>
	</section>