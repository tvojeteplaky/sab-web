<div class="pagination-centered">
    <ul class="pagination">
    <li class="arrow"><a href="<?php echo HTML::chars($page->url($current_page-1)) ?>">&laquo;</a></li>

        <li class="arrow"><a href="<?php echo HTML::chars($page->url($current_page+1)) ?>">&raquo;</a></li>
    </ul>
    <!-- .pagination -->
</div>