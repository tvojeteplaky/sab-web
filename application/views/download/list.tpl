<div class="banner" id="banner_download">
    	
    		<div class="row">
    		
    			<h1>{$item.nadpis}</h1>
    		
    		</div>
    	
    	</div>
    	
    	<!-- #################################################################################################################################################
    	
    		SECTION BREAK
    	
    	################################################################################################################################################# -->
    {foreach from=$downloads item=download key=key name=name}
        <div class="reveal" id="{$download.name}" data-reveal>
        <h1>{$download.nazev}</h1>
        <table>
        {foreach from=$download.files item=file key=key name=name}
            <tr>
                <td><img src="{$file.file_thumb}" alt="{$file.ext}"></td><td>{$file.nazev} ({$file.ext})</td><td class="text-right"><a href="{$file.file}" download=""><img src="{$media_path}img/downloadIco.png" alt="Download">&nbsp;&nbsp;&nbsp;Stáhnout hned</a></td>
            </tr>
        {/foreach}
        </table>
        <button class="close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    {/foreach}
    
    	<section class="whySAB">
          <div class="links row small-up-1 medium-up-2 large-up-3 wow slideInLeft">
          
    	{foreach from=$downloads item=download key=key name=download_boxes}
            <article class="column download_box" id="download_box_{$smarty.foreach.download_boxes.iteration}" {if !empty($download.photo.ad)}style="background-image:url('{$download.photo.ad}');" {/if}>
    
                <h3>
    				<a href="#" data-open="{$download.name}">
    					{$download.nazev}
    				</a>
    			</h3>
              
            </article>
            {/foreach}
    	
          </div>
{static_content code="modry_box"}

    	</section>