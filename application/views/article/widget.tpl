{if !empty($items)}
<aside class="whySAB wow slideInLeft">
				<div class="row">
					<div class="small-12 columns">
						<h2><span class="bold_text">Zprávy a aktuality</span></h2>
					</div>
				</div>
				<div class="row" data-equalizer>
				{foreach $items as $item name=iter}
					<div class="part_50 news_box_shadow margin_left_news_box" data-equalizer-watch>
						<div class="news_box" >
							<h3>
								{$item.nazev}
							</h3>
							<p class="justify">
								{$item.uvodni_popis}
		
							</p>
						</div>
					</div>
					{/foreach}
					
				</div>
				<div class="row">
					<div class="small-12 small-centered columns text-center">
						<a href="{$url_base}zpravy" class="button secondary wow slideInLeft">Více novinek</a>
					</div>
				</div>
			</aside>
{/if}