<div class="banner" id="banner_zpravy">
		
			<div class="row">
			
				<h1>{$item.nadpis}</h1>
			
			</div>
		
		</div>
		
		<!-- #################################################################################################################################################
		
			SECTION BREAK
		
		################################################################################################################################################# -->
		
		
		<section id="gallery_base_new_news" class="sector" style="margin-bottom: 60px;">
		
			<div class="row title_box classical_pad">
					<div class="small-12 columns">
				<h2 class="title_decor"><span class="bold_text">Nejnovější zprávy</span></h2>
			</div>              
			
			</div>
				
			<div class="row small-up-1 medium-up-3 large-up-3" data-equalizer>
				{foreach from=$news item=new key=key name=name}
				<article class="column" >    
				  
					<div class="gallery_photo_box" id="gallery_new_news_item_1" style="background-image: url('{$new.photo.t1}');" data-equalizer-watch>
					
						<div class="row top_sector">
						
							<a href="{$new.category.nazev_seo}" class="gallery_button">{$new.category.nazev}</a>
						
						</div>
						
						<div class="row dark_sector">
							
							<p>
							
								{$new.nazev}
								
							</p>
							
							<p>
								
								{$new.date|date_format:"%e.%m.%Y"}
								
							</p>
							
						</div>
					
					</div>
				
					<p  class="gallery_photo_box_p_white justify">
				
						{$new.uvodni_popis|strip_tags:true}
			
					</p>
			  
				</article>
					
				{/foreach}

	
			</div>
		
		</section>
		
		
		
		<!-- - - - - - - - - - - - - - - - - - - - - -  -->
		
		
		{if !empty($prefered)}
			
		
		<section id="gallery_base_other_news" class="sector">
		
			<div class="row title_box">
			
			<h2 class="title_decor"><span class="bold_text">Další zprávy</span></h2>
			
			</div>
				{foreach from=$prefered item=new key=key name=pref}
				{if $smarty.foreach.pref.first || $smarty.foreach.pref.index%3 == 0}
					<div class="row small-up-1 medium-up-3 large-up-3" data-equalizer>
				{/if}
				<article class="column">
				 
					<div class="gallery_photo_box" id="gallery_new_news_item_1" style="background-image: url('{$new.photo.t1}');" data-equalizer-watch>
					
						<div class="row top_sector">
						
							<a href="{$new.category.nazev_seo}" class="gallery_button">{$new.category.nazev}</a>
						
						</div>
						
						<div class="row dark_sector">
							
							<p>
							
								{$new.nazev}
								
							</p>
							
							<p>
								
								{$new.date|date_format:"%e.%m.%Y"}
								
							</p>
							
						</div>
					
					</div>
				
					<p  class="gallery_photo_box_p_white justify">
				
						{$new.uvodni_popis|strip_tags:true}
			
					</p>
		  
				</article>
					{if $smarty.foreach.pref.last || $smarty.foreach.pref.iteration%3 == 0}
						</div>
					{/if}
				{/foreach}
				<div class="row">
					<div class="right">{$pagination}</div>
				</div>
		</section>
		{/if}
{* 
	   <div class="row wow slideInLeft" id="information_box">
		<div class="large-7 columns">
		  <h3>
			Zajímají Vás bližší informace, nebo se chcete stát<br />
			novým klientem? <span class="text_light">Dejte nám vědět!</span>
		  </h3>
		</div>
		<div class="large-5 columns">
		  <a href="kontakt.html" class="buttonik">Napište nám</a>
		  <span class="information_item_move"><span class="or">nebo</span> +420&nbsp;<span class="bold_text">123&nbsp;456&nbsp;789</span></span>
		</div>
	
	  </div> *}
