<form action="?" method="post" data-abide novalidate>
	<div class="form-step-0">
		<div class="row">
			<div class="small-12 columns">
			
				<p>Zaujaly Vás naše služby? Chtěli byste s námi začít spolupracovat? Kontaktujte nás a my se Vám obratem ozveme.</p>
		 	</div>
		</div>
	</div>
	<div class="form-step-1 hide">
		<div class="row">
			<div class="small-12 columns">
				<label>
					<select name="contactform[typ_klienta]" required id="typKlienta">
						<option value="" disabled selected>Typ klienta*</option>
						<option value="Právnická osoba">Právnická osoba</option>
						<option value="Fyzická osoba podnikající">Fyzická osoba podnikající</option>
						<option value="Fyzická osoba nepodnikající">Fyzická osoba nepodnikající</option>
					</select>
				</label>
			</div>
		</div>
	</div>

	<div class="form-step-2 hide">
		<div class="row">
			<div class="medium-6 columns">
				<label>
					<input type="text" id="jmeno" placeholder="Jméno a příjmení*" value="{if isset($data.jmeno)}{$data.jmeno}{/if}" name="contactform[jmeno]" class="{if isset($errors.jmeno)}error{/if}" required >
					<span class="form-error">
					         Vyplňte prosím Vaše jméno a příjmení.
					</span>
				</label>
			</div>
			<div class="medium-6 columns">
				<label>
					<input type="text"  id="ico" placeholder="IČO*" value="{if isset($data.ico)}{$data.ico}{/if}" pattern="ico" name="contactform[ico]" class="form-step-2-b hide {if isset($errors.ico)}error{/if}" required>
					<span class="form-error">
					         Vyplňte prosím Vaše IČO.
					</span>
				</label>	
			</div>
		</div>
		
		<div class="row">
			<div class="medium-6 columns">
				<label>
					<input id="email" type="email" placeholder="E-mail*"  value="{if isset($data.email)}{$data.email}{/if}" name="contactform[email]" class="{if isset($errors.email)}error{/if}" required>
					<span class="form-error">
					          Vyplňte prosím Váš e-mail, nezapoměňte na @.
					</span>
				</label>
			</div>
			<div class="medium-6 columns">
				<label>
					<input id="tel" type="tel" placeholder="Telefon*" pattern="tel" value="{if isset($data.tel)}{$data.tel}{/if}" name="contactform[tel]" class="{if isset($errors.tel)}error{/if}" required>
					<span class="form-error">
					          Vyplňte prosím Vaše telefonní číslo ve formátu: +420 123 456 789.
					</span>
				</label>	
			</div>
		</div>
		<div class="row">
			<div class="medium-12 columns">
				<label>
					<select name="contactform[place]" id="place" required>
						<option value="" disabled selected>Místo schůzky*</option>
						<option value="Hlavní město Praha">Hlavní město Praha</option>
						<option value="Středočeský kraj">Středočeský kraj</option>
						<option value="Jihočeský kraj">Jihočeský kraj</option>
						<option value="Plzeňský kraj">Plzeňský kraj</option>
						<option value="Karlovarský kraj">Karlovarský kraj</option>
						<option value="Ústecký kraj">Ústecký kraj</option>
						<option value="Liberecký kraj">Liberecký kraj</option>
						<option value="Královéhradecký kraj">Královéhradecký kraj</option>
						<option value="Pardubický kraj">Pardubický kraj</option>
						<option value="Kraj Vysočina">Kraj Vysočina</option>
						<option value="Jihomoravský kraj">Jihomoravský kraj</option>
						<option value="Olomoucký kraj">Olomoucký kraj</option>
						<option value="Zlínský kraj">Zlínský kraj</option>
						<option value="Moravskoslezský kraj">Moravskoslezský kraj</option>
					</select>
					<span class="form-error">
					          Zvolte prosím místo schůzky.
					</span>
				</label>
			</div>
		</div>
		<div class="row form-step-2-a">
			<div class="medium-6 columns">
				<label>
					<select name="contactform[cetnost]" id="cetnost" required>
						<option value="" disabled selected>Četnost směny*</option>
						<option value="Jednorázově">Jednorázově</option>
						<option value="Pravidelně">Pravidelně</option>
					</select>
					<span class="form-error">
					          Vyberte prosím četnost směny.
					</span>
				</label>
			</div>
			<div class="medium-6 columns">
				<label>
					<select name="contactform[ucel]" id="ucel" required>
						<option value="" disabled selected>Účel směny*</option>
						<option value="Úspory">Úspory</option>
						<option value="Příjem ze zaměstnání">Příjem ze zaměstnání</option>
						<option value="Investice">Investice</option>
						<option value="Jiné">Jiné</option>
					</select>
					<span class="form-error">
					          Vyberte prosím účel směny.
					</span>
				</label>
			</div>
		</div>
		<div class="row">
			<div class="medium-12 columns">
				<label>
					<select name="contactform[objem]" id="objem" required>
						<option value="" disabled selected>Měsíční objem v CZK*</option>
						<option value="0 až 10 000"> 0 až 10 000</option>
						<option value="10 000 až 100 000">10 000 až 100 000</option>
						<option value="100 000  až 1 000 000">100 000  až 1 000 000</option>
						<option value="1 000 000  a více">1 000 000  a více</option>
					</select>
					<span class="form-error">
					          Vyberte prosím měsíční objem.
					</span>
				</label>
			</div>
		</div>
	</div>
	<div class="row form-step-1 hide">
		<div class="small-12 columns right">
			<p style="font-size: 0.7rem">* povinné pole</p>
		</div>
	</div>
	<div class="row">
		
		<div class="large-7 columns">
			<div class="info hide">
				<img src="{$media_path}img/item_infor_warning.png" alt="i">Děkujeme, budeme Vás kontaktovat.
			</div>
			<div class="info alert" data-abide-error style="display: none;">
				Prosíme o vyplnění chybějících údajů.
			</div>
		</div>
		<div class=" large-5 columns ">
			<label>
				<button type="button" class="button expanded continue">Pokračovat</button>
				<button type="submit" class="button expanded hide">Odeslat zprávu</button>
			</label>				
		</div>
	</div>
	
	{hana_secured_post action="send" module="contact"}
</form>
<div class="reveal" id="contactModal" data-reveal style="text-align: center;font-size:1.2rem">
  <p>Děkujeme za Váš zájem. Rádi Vás budeme kontaktovat.</p>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>

{* <form action="?" method="post">
				<div class="row">
					<div class="medium-6 columns">
						<label>
							<input type="text" placeholder="Jméno a příjmení"value="{if isset($data.jmeno)}{$data.jmeno}{/if}" name="contactform[jmeno]" class="{if isset($errors.jmeno)}error{/if}" required>
						</label>
					</div>
					<div class="medium-6 columns">
						<label>
							<input type="tel" placeholder="Telefonní číslo" name="contactform[tel]" required aria-required="true" value="{if isset($data.tel)}{$data.tel}{/if}" class="{if isset($errors.tel)}error{/if}">
						</label>
					</div>
				</div>
				<div class="row">
					<div class="medium-6 columns">
						<label>
							<input type="email" placeholder="Emailová adresa"  required aria-required="true" value="{if isset($data.email)}{$data.email}{/if}" name="contactform[email]" class="{if isset($errors.email)}error{/if}">
						</label>
					</div>
					<div class="medium-6 columns">
						<div class="info hide">
							<img src="{$media_path}img/item_infor_warning.png" alt="i">Děkujeme Vám, ozveme se brzy...
						</div>
					</div>
				</div>
				<div class="row">
					<div class="small-12 columns">
						<label>
							<textarea name="contactform[zprava]"  placeholder="Předvyplněno: Dobrý den, měli bychom zájem o Vaše služby, mohli byste nám zaslat Vaši nabídku?" >{if isset($data.zprava)}{$data.zprava}{/if}</textarea>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="large-4 large-offset-8 columns">
						<label>
							<button type="submit" class="button expanded">Odeslat zprávu</button>
						</label>
					</div>
				</div>{hana_secured_post action="send" module="contact"}
			</form> *}
{* <form class="contact-form text-left" action="?" method="post">
	<div class="row">
		<div class="medium-{if $controller == "contact"}4{else}12 large-5{/if} small-12 columns">
			<div class="row">
				<div class="small-12 column">
					<input type="text" required aria-required="true" value="{if isset($data.jmeno)}{$data.jmeno}{/if}" name="contactform[jmeno]" placeholder="Jméno a příjmení*" class="{if isset($errors.jmeno)}error{/if}">
				</div>
			</div>
			<div class="row">
				<div class="small-12 column">
					<input type="email" required aria-required="true" value="{if isset($data.email)}{$data.email}{/if}" name="contactform[email]" placeholder="Emailová adresa*" class="{if isset($errors.email)}error{/if}">
				</div>
			</div>
			<div class="row">
				<div class="small-12 column">
					<input type="tel" name="contactform[tel]" required aria-required="true" value="{if isset($data.tel)}{$data.tel}{/if}" placeholder="Telefonní číslo*" class="{if isset($errors.tel)}error{/if}">
				</div>
			</div>
			<div class="row">
				<div class="small-12 column">
					<div id="g-recaptcha"></div>
				</div>
			</div>
		</div>
		<div class="medium-{if $controller == "contact"}8{else}12 large-7{/if} small-12 columns">
			<textarea name="contactform[zprava]" placeholder="Text vaší zprávy..." required aria-required="true" class="{if isset($errors.zprava)}error{/if}">{if isset($data.zprava)}{$data.zprava}{/if}</textarea>
		</div>
	</div>
	<div class="row">
		<div class="medium-{if $controller == "contact"}8{else}12 large-7{/if} medium-offset-{if $controller == "contact"}4{else}0 large-offset-5{/if} small-offset-0 columns">
			<button type="submit" class="success button">{translate str="Odeslat zprávu"}</button>Položky označené * jsou povinné
		</div>
	</div>
	<div class="row">
		<div class="small-12 column">
			{hana_secured_post action="send" module="contact"}
		</div>
	</div>
</form>
<div class="tiny reveal" id="captcha-modal" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">
	<p>Prosím ověřte, že nejste robot.</p>
	<button class="close-button" data-close aria-label="Close reveal" type="button">
		<span aria-hidden="true">&times;</span>
	</button>
</div>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
		async defer>
</script> *}