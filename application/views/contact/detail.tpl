<div class="banner" id="banner_kontakty">
		
			<div class="row">
			
				<h1>Kontaktujte nás</h1>
			
			</div>
		
		</div>
		
		<section id="rychly_kontakt">
		
			<div class="row" data-equalizer="contact_box" data-equalizer="text">
				{foreach from=$branches item=branch key=key name=name}
					<div class="large-6 columns">
						<div style="background-image: url('{$branch.photo.t1}')">
							<article class="kontakty_box" >
		
								<div data-equalizer-watch="contact_box">
								<h3>{$branch.nadpis}</h3>
								{$branch.address}
								</div>
								<a href="{$branch.google_url}" target="_blank" class="button">Zobrazit na mapě</a>
		
							</article>
						</div>
					</div>
					
				{/foreach}
			
			</div>
		
		</section>
	
	
	<div class="row wow slideInLeft">
		<div class="small-12 columns">
			<ul class="accordion" data-accordion data-allow-all-closed="true">
			{foreach from=$parts item=part key=key name=name}
				<li class="accordion-item " data-accordion-item>
					<a href="#" class="accordion-title">{$part.nazev}</a>
					<div class="accordion-content" data-tab-content>
						<div class="row">
							<div class="small-12 columns">
								{$part.popis}
							</div>
						</div>
					</div>
				</li>
				
			{/foreach}
			</ul>
		</div>
	</div>
	
	<section class="row" id="contactForm">

		<div class="small-12 columns wow slideInLeft">
		<div class="row">
			<div class="small-12 columns">
					<h3>Máte zájem o naše služby?</h3>
			</div>
		</div>
		{widget controller="contact" action="show"}

		</div>
	</section>