<div class="reveal blue wow slideInLeft" id="zajem" data-reveal>
      <div class="row">
        <div class="small-12 columns">
          <h1>Máte zájem o naše služby?</h1>
        </div>
      </div>
    {widget controller="contact" action="show"}
    
      <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>