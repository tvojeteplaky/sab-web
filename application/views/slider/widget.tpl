{if !empty($sliders) && !empty($sliders[0]['slides'])}
{foreach $sliders as $slider}
 <div id="slider" class="orbit homepageSlider" role="region" aria-label="Bannery" data-orbit>
      <ul class="orbit-container">
      {foreach $slider.slides as $slide name=slides}
      {if $slide.photo_src}
        <li class="orbit-slide {if $smarty.foreach.slides.first}is-active{/if}">
            <img class="orbit-image" data-interchange="[{$media_path}photos/slider/item/images-{$slide.id}/{$slide.photo_src}-ad.jpg, small], [{$media_path}photos/slider/item/images-{$slide.id}/{$slide.photo_src}-t1.jpg, medium], [{$media_path}photos/slider/item/images-{$slide.id}/{$slide.photo_src}-t2.jpg, xxlarge]" alt="{$slide.nazev}">
            <div class="row orbitText">
                <div class="small-12 columns">
                    {if $slide.nadpis}
                        <h2>{$slide.nadpis}</h2>
                    {/if}
                    {$slide.text}
                    {if $slide.button_text}
                    <a href="{$slide.link}" class="button">{$slide.button_text}</a>
                    {/if}
                </div>
            </div>
            
        </li>
        {/if}
        {/foreach}
        
        
    </ul>
    
    {if count($slider.slides) > 1}
      <nav class="orbit-bullets">
        {foreach $slider.slides as $slide name=slides}
                        <button {if $smarty.foreach.slides.first}class="is-active"{/if} data-slide="{$smarty.foreach.slides.index}"></button>
        {/foreach}
      </nav>
      {/if}
    </div>
    {/foreach}
{/if}