<div class="banner" id="banner_{if $item.nazev_seo eq "o-nas"}o_spolecnosti{elseif $item.nazev_seo eq "pro-klienty"}pro_klienty{else}kariera{/if}">
		
			<div class="row">
			
				<h1>{$item.nadpis}</h1>
			
			</div>
		
		</div>
		{if !empty($item.popis) && !empty($item.photo.t1) }
			   
	<section style="float: left; width: 100%;margin-top:1.5rem;" class="slovakFix">
	
		<div class="row">
			<div class="medium-6 columns">
				<p><br><br></p>
			   {$item.popis}
			</div>
			<div class="medium-6 columns">
				<img src="{$item.photo.t1}" alt="{$item.nazev}" style="border-radius: 5px;">
			</div>
		</div>
	
	</section>
		{elseif !empty($item.popis)}
		<section style="float: left; width: 100%;" class="slovakFix">
	
			<div class="row">
				<div class="small-12 columns">
					<p><br><br></p>
				   {$item.popis}
				</div>
				
			</div>
	
		</section>
		{/if}
		
{foreach from=$item.childs item=child key=key name=name}
		{capture assign="box"}
			{widget controller="box" action="structured_box" param=$child.id}
		{/capture}
			{if  strlen(trim($box)) > 0}
				{$box}
			{elseif !empty($child.photos)}
				<section>
					<div class="row title_box wow slideInLeft">
						<div class="small-12 column">
						<h2 class="title_decor" id="{$child.nazev_seo}">{$child.nazev}</h2>
						</div>
					</div>
				</section>
				<section>
					<div class="row wow slideInLeft">
						<div class="small-12">
							<ul  id="tym-slider">
							{foreach from=$child.photos item=photo key=key name=name}
								<li>
									<div><a href="{$photo.t2}" data-lightbox="tym-slider" data-title="{$photo.popis}"><img src="{$photo.t1}" alt="{$photo.nazev}"></a></div>
								</li>
							{/foreach}
								
							</ul>
						</div>
					</div>
				</section>
			{elseif !empty($child.photo) && !empty($child.popis)}
			<section class="o-spolecnosti_banner" style="background-image: url('{$child.photo.t2}')">
				<div class="row wow slideInLeft">
					<div class="row title_box">
					<div class="small-12 column">
						<h2 class="title_decor" id="{$child.nazev_seo}">{$child.nadpis}</h2>
					</div>
					</div>
				</div>
				<div class="row">
					<div class="medium-6  columns">
						 {$child.popis}
					</div>
					<div class="medium-6 columns">
						{$child.uvodni_popis}
					</div>
				</div>

					
				{if !empty($child.url) }
					<div class="row center">
						<a href="{$child.url}" class="button">Zjistěte více</a>
					</div>
				{/if}
			</section>
			{/if}
{/foreach}
{* <div id="page">

	
{/foreach}
{$item.popis}
	</div> *}
