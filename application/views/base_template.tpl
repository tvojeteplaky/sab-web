{*
CMS system Hana ver. 2.6 (C) Pavel Herink 2012
zakladni spolecna sablona layoutu stranek

automaticky generovane promenne spolecne pro vsechny sablony:
-------------------------------------------------------------
	$url_base      - zakladni url
	$tpl_dir       - cesta k adresari se sablonama - pro prikaz {include}
	$url_actual    - autualni url
$url_homepage  - cesta k homepage
	$media_path    - zaklad cesty do adresare "media/" se styly, obrazky, skripty a jinymi zdroji
$controller
$controller_action
$language_code - kod aktualniho jazyku
$is_indexpage  - priznak, zda jsme na indexove strance

	$web_setup     - pole se zakladnim nastavenim webu a udaji o tvurci (DB - admin_setup)
	$web_owner     - pole se zakladnimi informacemi o majiteli webu - uzivatelske informace (DB owner_data)
-------------------------------------------------------------

doplnujici custom Smarty funkce
-------------------------------------------------------------
									{translate str="nazev"}                                      - prelozeni retezce
					{static_content code="index-kontakty"}                       - vlozeni statickeho obsahu
	{widget name="nav" controller="navigation" action="main"}    - vlozeni widgetu - parametr "name" je nepovinny, parametr "action" je defaultne (pri neuvedeni) na hodnote "widget"
		{hana_secured_post action="add_item" [module="shoppingcart"]}        nastaveni akce pro zpracovani formulare (interni overeni parametru))
{hana_secured_multi_post action="obsluzna_akce_kontroleru" [submit_name = ""] [module="nazev_kontoleru"]}
{$product.cena|currency:$language_code}
{hana_secured_get action="show_suggestions" module="search"} h_module=search&h_action=show_suggestions&h_secure=60c6c1049b070ce38018671b704955df

Promenne do base_template:
-------------------------------------------------------------
{$page_description}
{$page_keywords}
{$page_name} - {$page_title}
{$main_content}
{include file="`$tpl_dir`dg_footer.tpl"}
{if !empty($web_owner.ga_script)}
	{$web_owner.ga_script}
	{/if}


*}
<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="author" content="{$web_setup.nazev_dodavatele}"/>
	<meta name="copyright" content="{$web_owner.copyright}" />
	<meta name="keywords" content="{$page_keywords}"/>
	<meta name="description" content="{$page_description} "/>
	<meta name="robots" content="all,follow"/>
	<meta name="googlebot" content="snippet,archive"/>
	<title>{$page_title} &bull; {$page_name}</title>
	<link href='{$media_path}css/app.css' rel='stylesheet' type='text/css'>
</head>
<body>
<div id="loader"><img src="{$media_path}img/logoSab.png" alt="sab"></div>
	{widget controller=navigation action=main}

	{$main_content}
	<footer>

	 {widget controller=newsletter}
		<div id="footer_contact_section">
			<div class="row text-center medium-text-left">
				<div class="medium-4 large-3 columns">
					<ul>
						<li class="bold_text">Sídlo společnosti - Praha</li>
						<li>SAB Finance a.s.</li>
						<li>Senovážné nám. 1375/19</li>
						<li>110 00 Praha 1 - Nové Město</li>
					</ul>
				</div>
				<div class="medium-3 large-2 columns">
					<ul>
						<li class="bold_text">Provozovna - Zlín</li>
						<li>SAB Finance a.s.</li>
						<li>Sadová 7</li>
						<li>760 01 Zlín</li>
					</ul>
				</div>
				<div class="medium-3 large-2 columns end">
					<ul>
						<li class="bold_text">IČ:  24717444</li>
						<li class="bold_text">DIČ: CZ699003130</li>
					</ul>
				</div>
			</div>
		</div>

	</footer>


	<script type="text/javascript" src="{$media_path}js/app.js"></script>
	{literal}
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', '{/literal}{$web_owner.ga_script}{literal}']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	{/literal}
{widget controller="site" action="message"}
</body>
</html>