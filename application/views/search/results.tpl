{capture assign=subnav}
    {widget controller=page action=subnav}
{/capture}
<div class="detail" id="search">
    <header class="text-center {if $item.photo_src}with-photo{/if}">
        {if $item.photo_src}
            <img alt="{$item.nazev} photo"
                 data-interchange="[{$media_path}photos/page/{if $item.page_category_id == 2}unrelated{else}item{/if}/images-{$item.id}/{$item.photo_src}-header-sm.jpg, small], [{$media_path}photos/page/{if $item.page_category_id == 2}unrelated{else}item{/if}/images-{$item.id}/{$item.photo_src}-header.jpg, large]">
        {/if}
        <div class="heading">
            <div class="row">
                <div class="small-12 columns">
                    <h1>{$item.nadpis|bbcode}</h1>
                </div>
            </div>
        </div>
    </header>
    <div class="row main">
        {if strlen(trim($subnav)) > 0}
            <div class="medium-3 small-12 columns">
                {$subnav}
            </div>
        {/if}
        <div class="medium-{if strlen(trim($subnav)) > 0}9{else}12{/if} small-12 columns">
            {$item.popis}
            <p>
                Hledaný výraz: <strong>"{$keyword}"</strong>
            </p>

            <div class="row">
                {foreach from=$search_results key=key item=results name=search}
                    {foreach $results as $result}
                        <div class="small-12 column">
                            <a href="{$url_base}{$result.nazev_seo}">
                                <div class="transparent media-box">
                                    <h5 class="highlight">{$result.title}</h5>
                                    {$result.text}
                                </div>
                            </a>
                        </div>
                    {/foreach}
                {/foreach}
            </div>
            {if $item.show_contactform}
                <div class="contact-wrapper">
                    <h4>Máte zájem o naše služby?</h4>
                    {widget controller="contact" action="show"}
                </div>
            {/if}
        </div>
    </div>
</div>
{*
<section id="search" class="detail">
    <header class="header-bg">
        <div class="row">
            <div class="small-12 column text-center">
                <h1>{$item.nadpis}</h1>
            </div>
        </div>
    </header>
    <div class="row">
        <div class="small-12 columns">
            <ul class="small-block-grid-1">
                {foreach from=$search_results key=key item=results name=search}
                    {foreach $results as $result}
                        <li>
                            <h3 class="title">{$result.title}</h3>
                            {$result.text}
                            <a href="{$url_base}{$result.nazev_seo}" class="button">
                                {translate str="Pokračovat"}
                            </a>
                        </li>
                    {/foreach}
                {/foreach}
            </ul>
        </div>
    </div>
</section>
*}