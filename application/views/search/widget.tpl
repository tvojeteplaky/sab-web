<form action="{$url_base}{translate_route route="vyhledavani"}" method="post">
    <div class="row small-collapse" id="search-widget">
        <div class="medium-10 small-9 columns">
            <input type="search" name="q" placeholder="{translate str="Vyhledávání"}...">
        </div>
        <div class="medium-2 small-3 columns">
            <button type="submit"></button>
        </div>
    </div>
</form>