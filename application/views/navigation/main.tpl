{if !empty($links)}
{capture assign=subnav}
    {widget controller=$controller action=subnav}
{/capture}
<div data-sticky-container id="topBar">
	  <div data-sticky data-options="marginTop:0;stickyOn: small;" style="width:100%">
		<div class="title-bar responsiveToggle" data-responsive-toggle="topBarMenu" data-hide-for="large">
		  <button class="menu-icon" type="button" data-toggle></button>
		  <div class="title-bar-title">Menu</div>
		</div>
		<div class="title-bar" id="topBarMenu">
		  <div class="topBar">
			<div class="row">
			  <div class="small-12 columns">
				<div class="top-bar-title">
				  <a href="{$url_homepage}"><img src="{$media_path}img/logoSab.png" alt="sab"></a>
				</div>
				<div>
				  <div class="title-bar-left"><!-- Content --></div>
				  <div class="title-bar-right">
					<ul class="vertical large-horizontal menu">
					{foreach from=$links item=link key=key}
					  <li class="{if array_key_exists($link.nazev_seo, $sel_links)}active{/if}"><a href="{$url_base}{$link.nazev_seo}">{if $link.nazev_seo != "index"}{$link.nazev}{else}<img src="{$media_path}img/home.png" alt="{$link.nazev}">{/if}</a></li>
					{/foreach}
					  <li class="noHover"><a href="#" class="button" data-open="zajem">Mám zájem</a></li>
					</ul>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
		{if strlen(trim($subnav)) > 0}
			{$subnav}
		{/if}
	  </div>
</div>
{/if}
{widget controller="contact" action="show_demand"}