{if !empty($items)}
<div id="sub_menu" class="hide-for-small-only">
    
    			<div class="row">
    {foreach $items as $item}
    				<a href="{$url_base}{$item.nazev_seo}" class="sub_menu_a {if !empty($sel_links) and array_key_exists($item.nazev_seo, $sel_links)}actived{/if}">{$item.nazev}</a>
    			{/foreach}

    
    			</div>
    
    		</div>
{/if}
