<form action="#" method="post" id="form" class="newsletter">
        <div class="sector" id="newsletter_section">
          <div class="row newsLetter">
            <div class="large-6 columns">
              <h2 class="white_text">Přihlásit se k odběru novinek</h2>
    
              <p style="font-size: 0.9rem;">
                Z odebírání novinek se můžete kdykoliv odhlásit.
                Kontaktní údaje nikdy nebudou poskytnuty třetí straně.
              </p>
            </div>
            <div class="large-5 columns">
              <input type="text" name="kontrolni_cislo" class="hide">
              <div class="input-group">
                <input class="input-group-field" name="newsletter_email" type="email" placeholder="Vložte Vaši e-mailovou adresu">
                <div class="input-group-button">
                  <input type="submit" class="button" value="ok">
                </div>
              </div>
            </div>
          </div>
        </div>
        {hana_secured_post action="subscribe_to_newsletter" module="newsletter"}
    </form>
    <div class="reveal" id="newsletterModal" data-reveal>
  <p>Váš e-mail byl v pořádku přidán, děkujeme Vám.</p>
  <button class="close-button" data-close aria-label="Close modal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>