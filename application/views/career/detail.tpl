<div class="banner" id="banner_kariera">
		
			<div class="row">
			
				<h1>{$page.nadpis}</h1>
			
			</div>
		
		</div>
			
		<section  class="sector koho wow slideInLeft">
		
			<div class="row">
						  
				<div class="small-12 columns">
				
					<h2 class="title_decor">
						{$page.podnadpis}
					</h2>
					
				</div>
			</div>
				
				
			<div class="row">
				<div class="small-12 column">
					<p>
						{$page.uvodni_popis}
					</p> 
				</div>
			</div>
			<div class="row">
	    			<div class="large-6 large-push-6 columns">
	    				<img src="{$page.photo.t1}" alt="{$page.nadpis}" style="border-radius: 5px;">
	    			</div>
	    			<div class="large-6 large-pull-6 columns">
	    				{$page.popis}
	    			</div>
	    		</div>
    	
		
		</section>

		{foreach from=$page.childs item=child key=key name=name}
		{capture assign="box"}
			{widget controller="box" action="structured_box" param=$child.id}
		{/capture}
			{if  strlen(trim($box)) > 0}
				{$box}
			{elseif !empty($child.photo) && !empty($child.uvodni_popis) && !empty($child.popis)}
			<aside class="sliderik classical_pad sector wow slideInLeft" id="staze_slider" style="margin-top: 60px;background-image: url('{$child.photo.t2}')">
		
			<div class="row" id="staze-pro-studenty" >
			
				{*<div class="part_70">
					
						<h2 class="slider_title">{$child.nadpis}</h2>
											
						{$child.popis}
					
				</div>
					
				<div class="part_30">
					
						
						
							{$child.uvodni_popis}
	
				
				</div>*}

				<div class="large-9 medium-8 columns">
					<h2 class="slider_title">{$child.nadpis}</h2>
					{$child.popis}
					
				</div>
				<div class="large-3 medium-4 columns">
					{$child.uvodni_popis}
				</div>
				
			</div>
		
		</aside>
			{else}
			<div id="{$child.nazev_seo}"></div>
			<div class="row wow slideInLeft" id="volne-pozice">
				<div class="small-12 columns">
					<ul class="accordion" data-accordion data-allow-all-closed="true" data-multi-expand="true">
						{foreach from=$careers item=career key=key name=name}
							<li class="accordion-item" data-accordion-item>
							<a href="#" class="accordion-title">{$career.nazev}</a>
							<div class="accordion-content" data-tab-content>
								{$career.uvodni_popis}
								<div class="row">
									<div class="medium-6 columns">
									{$career.popis}
									</div>

									<div class="medium-6 columns">
										<div class="row">
											<div class="medium-12 large-8 large-offset-4 columns blueInfo">
			 									{$career.kontakty}
											</div>
										</div>
									</div>
								</div>
							</div>
											  
						</li>
						{/foreach}
					</ul>
				</div>
			</div>
			{/if}
		{/foreach}


		
		
		<section class="sector">
		
			{static_content code="modry_box"}
		
		</section>